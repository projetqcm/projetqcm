package fr.eni.projetqcm.bll;

import java.util.List;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.JsonInscription;
import fr.eni.projetqcm.bo.Test;
import fr.eni.projetqcm.bo.Utilisateur;
import fr.eni.projetqcm.dal.DAOFactory;
import fr.eni.projetqcm.dal.EpreuveDAO;

public class EpreuveManager {
	private EpreuveDAO epreuveDAO;
	
	public EpreuveManager() {
		epreuveDAO = DAOFactory.getEpreuveDAO();
	}
	
	public List<Epreuve> getEpreuvesByUserIdAp(Utilisateur utilisateur) {
		return epreuveDAO.selectAllByUserIdAp(utilisateur);
	}
	
	public List<Epreuve> getEpreuvesByUserIdT(Utilisateur utilisateur) {
		return epreuveDAO.selectAllByUserIdT(utilisateur);
	}
	
	public List<Epreuve> selectAll(){
		return epreuveDAO.selectAll();
	}
	
	public Epreuve getEpreuveById(int id){
		return epreuveDAO.selectById(id);
	}
	
	public void insertEpreuve(JsonInscription jsonInscription){
		TestManager testManager = new TestManager();
		Test test = testManager.selectById(jsonInscription.getIdTest());
		
		for(Integer item : jsonInscription.getIdListUser()) {
			
			Epreuve epreuve = new Epreuve(jsonInscription.getDateDebutValidite(), jsonInscription.getDateFinValidite(), 
					0, "EA", null, null,test, item);

			epreuveDAO.insert(epreuve);
		}
	}
	
	public List<Epreuve> selectAllByIdTest(int test) {
		return epreuveDAO.selectAllByIdTest(test);
	}
	
}
