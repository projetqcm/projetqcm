package fr.eni.projetqcm.bll;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Question;
import fr.eni.projetqcm.bo.QuestionTirage;
import fr.eni.projetqcm.bo.ReponseTirage;
import fr.eni.projetqcm.bo.SectionTest;
import fr.eni.projetqcm.dal.DAOFactory;
import fr.eni.projetqcm.dal.EpreuveDAO;
import fr.eni.projetqcm.dal.QuestionDAO;
import fr.eni.projetqcm.dal.QuestionTirageDAO;
import fr.eni.projetqcm.dal.ReponseTirageDAO;

public class TirageManager {
	private Random rand = new Random();
	private EpreuveDAO eprDAO;
	private QuestionDAO qDAO;
	private QuestionTirageDAO qtDAO;
	private ReponseTirageDAO rtDAO;

	public TirageManager() {
		eprDAO = DAOFactory.getEpreuveDAO();
		qDAO = DAOFactory.getQuestionDAO();
		qtDAO = DAOFactory.getQuestionTirageDAO();
		rtDAO = DAOFactory.getReponseTirageDAO();
	}

	public List<QuestionTirage> getQuestionTirage(int eprId) {		
		List<Integer> tirageId = new ArrayList<Integer>();
		List<QuestionTirage> tirage = qtDAO.selectByEpreuveId(eprId);
		//System.out.println(qtDAO.selectByEpreuveId(eprId));

		if (tirage.isEmpty()) {
			Epreuve epr = eprDAO.selectById(eprId);
			int numOrdre = 1;
			for (SectionTest section : epr.getTest().getSections()) {
				//System.out.println(section.toString());
				List<Question> questions = qDAO.selectAllByTheme(section.getTheme());
				//System.out.println(questions);
				int i = 0;
				while (i < section.getNbQuestionsATirer()) {
					Question question = questions.get(rand.nextInt(questions.size()));
					QuestionTirage qTirage = new QuestionTirage(false, question, numOrdre, epr);
					if (!tirageId.contains(qTirage.getQuestion().getIdQuestion())) {
						//System.out.println("ajout");
						qtDAO.insert(qTirage);
						tirageId.add(qTirage.getQuestion().getIdQuestion());
						tirage.add(qTirage);
						i++;
						numOrdre++;
					} 
				}
			}
		}
		return tirage;
	}
	
	public List<ReponseTirage> getReponseTirage(int eprId) {
		//System.out.println(rtDAO.selectByEpreuveId(eprId));
		return rtDAO.selectByEpreuveId(eprId);
	}
}
