package fr.eni.projetqcm.bll;

import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.Test;
import fr.eni.projetqcm.dal.DAOFactory;
import fr.eni.projetqcm.dal.TestDAO;


public class TestManager {
	private TestDAO testDAO;
	
	public TestManager() {
		testDAO = DAOFactory.getTestDAO();
	}
	
	public List<Test> selectAll() {
		return testDAO.selectAll();
	}
	
	public Test selectById(Integer idTest) {
		return testDAO.selectById(idTest);
	}
	
}
