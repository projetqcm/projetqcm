package fr.eni.projetqcm.bll;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

import fr.eni.projetqcm.bo.Profil;
import fr.eni.projetqcm.bo.Promotion;
import fr.eni.projetqcm.bo.Utilisateur;
import fr.eni.projetqcm.dal.DAOFactory;
import fr.eni.projetqcm.dal.UtilisateurDAO;
import fr.eni.projetqcm.utils.PasswordUtils;

public class UtilisateurManager {
	private UtilisateurDAO utilisateurDAO;
	private Boolean erreurs = false;

	public UtilisateurManager() {
		this.utilisateurDAO = DAOFactory.getUtilisateurDAO();
	}

	public Utilisateur loginUtilisateur(HttpServletRequest request) {
		String email = request.getParameter("login");
		String password = request.getParameter("password");

		Utilisateur utilisateur = new Utilisateur();

		if (email == null || email.trim().length() == 0 || utilisateurDAO.selectByEmail(email) == null) {
			this.erreurs = true;
		} else {
			utilisateur = utilisateurDAO.selectByEmail(email);
		}

		// changer le salt par celui en base quand on aura créé des utilisateurs
		String salt = PasswordUtils.getSalt(30);
System.out.println(salt);
		if (password == null || password.trim().length() == 0) {
			this.erreurs = true;
		} else if (!PasswordUtils.verifyUserPassword(password,
				PasswordUtils.generateSecurePassword(utilisateur.getPassword(), salt), salt)) {
			this.erreurs = true;
		}

		return utilisateur;
	}

	public Boolean getErreurs() {
		return this.erreurs;
	}

	public void insertUtilisateur(HttpServletRequest request) {
		Profil profil = new Profil();
		profil.setCodeProfil(3);
		Promotion promotion = new Promotion();
		promotion.setCodePromo(request.getParameter("promotion_candidat"));

		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		String pwd = RandomStringUtils.random(5, characters);
		System.out.println(pwd);
		Utilisateur utilisateur = new Utilisateur(request.getParameter("name_candidat"),
				request.getParameter("prenom_candidat"), request.getParameter("email_candidat"), pwd, profil,
				promotion);

		utilisateurDAO.insert(utilisateur);
	}

	public List<Utilisateur> selectUtilisateurByMasque(String result, int idTest) {
		Utilisateur utilisateur = new Utilisateur();
		List<Utilisateur> listUtilisateur = new ArrayList<Utilisateur>();
		utilisateur = new Utilisateur(result, result);
		listUtilisateur = utilisateurDAO.selectByMasque(utilisateur, idTest);

		return listUtilisateur;
	}
}
