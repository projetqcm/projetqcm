package fr.eni.projetqcm.bll;

import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.Promotion;
import fr.eni.projetqcm.dal.DAOFactory;
import fr.eni.projetqcm.dal.PromotionDAO;

public class PromotionManager {
	
	private PromotionDAO promotionDAO;
	
	public PromotionManager() {
		promotionDAO = DAOFactory.getPromotionDAO();
	}
	
	public List<Promotion> selectAll() {
		return promotionDAO.selectAll();
	}
	
	public List<Promotion> selectPromotionByMasque(String promotion){
		List<Promotion> listPromotion = new ArrayList<Promotion>();
		listPromotion = promotionDAO.selectPromotionByMasque(promotion);
		return listPromotion;
	}
}
