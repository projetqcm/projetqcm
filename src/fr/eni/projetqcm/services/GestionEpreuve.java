package fr.eni.projetqcm.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Proposition;
import fr.eni.projetqcm.bo.QuestionTirage;
import fr.eni.projetqcm.bo.ReponseTirage;
import fr.eni.projetqcm.dal.DAOFactory;
import fr.eni.projetqcm.dal.EpreuveDAO;
import fr.eni.projetqcm.dal.QuestionTirageDAO;
import fr.eni.projetqcm.dal.ReponseTirageDAO;

@Path("/epreuve")
public class GestionEpreuve {
	QuestionTirageDAO qtDAO = DAOFactory.getQuestionTirageDAO();
	ReponseTirageDAO rtDAO = DAOFactory.getReponseTirageDAO();
	EpreuveDAO eprDAO = DAOFactory.getEpreuveDAO();

	@POST
	@Path("/reponse/ajouter")
	public void setReponse(@FormParam("idEpreuve") int idEpr, @FormParam("idQuestion") int idQuestion, @FormParam("idPropositions") String propositions) {
		//System.out.println("string json : " + propositions);
		List<Integer> idReponses = new Gson().fromJson(propositions, new TypeToken<ArrayList<Integer>>(){}.getType());				
		ReponseTirage rt = new ReponseTirage(new Proposition(), new QuestionTirage(), new Epreuve());
		rt.getEpreuve().setIdEpreuve(idEpr);
		rt.getQuestionTirage().getQuestion().setIdQuestion(idQuestion);
		
		QuestionTirage qt = new QuestionTirage();
		qt.getEpreuve().setIdEpreuve(idEpr);
		qt.getQuestion().setIdQuestion(idQuestion);
		List<Integer> idPropositions = new ArrayList<Integer>();
		for (Proposition prop : qtDAO.selectByQuestion(qt).getQuestion().getPropositions()) {
			idPropositions.add(prop.getIdProposition());
		}
		
		for (Integer idProposition : idPropositions) {
			//System.out.println(idProposition);
			rt.getProposition().setIdProposition(idProposition);
			//System.out.println("Réponses : " + idReponses + " propositions : " + idPropositions);
			if(idReponses.contains(idProposition)) {
				if(rtDAO.selectByReponse(rt) == null) {
					//System.out.println("insert");
					rtDAO.insert(rt);
				}
			} else {
				//System.out.println("delete");
				rtDAO.delete(rt);
			}
		}
	}

	@POST
	@Path("/question/marquee")
	public void setMarquage(@FormParam("idEpreuve") int idEpr, @FormParam("idQuestion") int idQuestion) {
		QuestionTirage qt = new QuestionTirage();
		qt.getEpreuve().setIdEpreuve(idEpr);
		qt.getQuestion().setIdQuestion(idQuestion);
		qt = qtDAO.selectByQuestion(qt);
		if(qt.getEstMarquee()) {
			qt.setEstMarquee(false);
			qtDAO.update(qt);
		} else {
			qt.setEstMarquee(true);
			qtDAO.update(qt);
		}
	}

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getEpreuve(@FormParam("idEpreuve") int idEpr) {
		return new Gson().toJson(eprDAO.selectById(idEpr));
	}
	
	@POST
	@Path("/timer")
	public void setTempsEcoulee(@FormParam("idEpreuve") int idEpr, @FormParam("tempsEcoule") int tempsEcoule) {
		Epreuve epr = eprDAO.selectById(idEpr);
		epr.setTempsEcoule(tempsEcoule);
		//System.out.println(epr);
		eprDAO.update(epr);		
	}
	
	@POST
	@Path("/etat")
	public void setEtat(@FormParam("idEpreuve") int idEpr, @FormParam("etat") String etat) {
		System.out.println("idEpreuve : " + idEpr + ", Etat : " + etat);
		Epreuve epr = eprDAO.selectById(idEpr);
		if(etat.equals("T")){
			List<QuestionTirage> qtList = qtDAO.selectByEpreuveId(idEpr);
			List<ReponseTirage> rtList = rtDAO.selectByEpreuveId(idEpr);
			Float note = 0f;
			Float totalNote = 0f;
			for (QuestionTirage qt : qtList) {
				Integer bonneReponse = 0;
				Integer bonneProposition = 0;
				for (Proposition proposition : qt.getQuestion().getPropositions()) {
					//System.out.println(proposition.getEstBonne());
					if(proposition.getEstBonne()) {
						bonneProposition++;
						for (ReponseTirage rt : rtList) {
							if(rt.getProposition().getQuestion().getIdQuestion().equals(proposition.getQuestion().getIdQuestion())) {
								if(rt.getProposition().getIdProposition().equals(proposition.getIdProposition()) && rt.getProposition().getEstBonne()) {
									bonneReponse++;
									//System.out.println("break");
								} else if (!rt.getProposition().getEstBonne()) {
									bonneReponse = 0;
									break;
								}
							}
						}
					}
				}
				
				if(bonneReponse != 0) {
					note += qt.getQuestion().getPoints() * (bonneReponse / bonneProposition);
				}
				totalNote += qt.getQuestion().getPoints();
				System.out.println("note : " + note + " total : " + totalNote);
			}
			
			note = Math.round(((note/totalNote)*20) * 100.0f) / 100.0f;
			epr.setNote_obtenue(note);
			if(epr.getTest().getSeuil_bas() > note) {
				epr.setNiveau_obtenu("NA");
			} else if (epr.getTest().getSeuil_haut() < note) {
				epr.setNiveau_obtenu("A");
			} else {
				epr.setNiveau_obtenu("ECA");
			}
			qtDAO.deleteByEpreuve(epr);
		}		
		epr.setEtat(etat);
		//System.out.println(epr);
		eprDAO.update(epr);
	}
}
