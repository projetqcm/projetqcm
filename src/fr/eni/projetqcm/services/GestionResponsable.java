package fr.eni.projetqcm.services;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import fr.eni.projetqcm.bll.EpreuveManager;
import fr.eni.projetqcm.bll.PromotionManager;
import fr.eni.projetqcm.bll.UtilisateurManager;
import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.JsonInscription;
import fr.eni.projetqcm.bo.ListUtilisateurBo;
import fr.eni.projetqcm.bo.Promotion;
import fr.eni.projetqcm.bo.QuestionTirage;
import fr.eni.projetqcm.bo.Utilisateur;
import fr.eni.projetqcm.dal.DAOFactory;
import fr.eni.projetqcm.dal.QuestionTirageDAO;

@Path("/responsable")
public class GestionResponsable {
	QuestionTirageDAO qtDAO = DAOFactory.getQuestionTirageDAO();
	HashMap<String, String> dataSearchCandidat = new HashMap<String, String>();
	List<Utilisateur> listUtilisateur = new ArrayList<Utilisateur>();
	SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.UK );
	
	@POST
	@Path("/searchcandidat")
	@Produces(MediaType.APPLICATION_JSON)
	public String setReponse(@FormParam("data") String dataSearchCandidat) {
		Map<String, String> map = new Gson().fromJson(dataSearchCandidat, new TypeToken<HashMap<String, String>>(){}.getType());
		if(!map.get("inputsearch").equals(null) && !map.get("inputsearch").trim().equals("")) {
			if(map.get("radiosearch").equals("name_search")){
				
				UtilisateurManager utilisateurManager = new UtilisateurManager();
				listUtilisateur = utilisateurManager.selectUtilisateurByMasque(map.get("inputsearch"), Integer.parseInt(map.get("idtest")));
				Gson gson = new Gson();
	
				// 1. Java object to JSON, and save into a file
				try {
					gson.toJson(listUtilisateur, new FileWriter("D:\\file.json"));
				} catch (JsonIOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				// 2. Java object to JSON, and assign to a String
				String jsonInString = gson.toJson(listUtilisateur);
	
				return jsonInString;
			}else {
				List<Promotion> listPromotion = new ArrayList<Promotion>();
				PromotionManager promotionManager = new PromotionManager();
				listPromotion = promotionManager.selectPromotionByMasque(map.get("inputsearch"));
	
				Gson gson = new Gson();
	
				// 1. Java object to JSON, and save into a file
				try {
					gson.toJson(listPromotion, new FileWriter("D:\\file.json"));
				} catch (JsonIOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// 2. Java object to JSON, and assign to a String
				String jsonInString = gson.toJson(listPromotion);
				
				return jsonInString;
			}
		}else {
			return null;
		}

	}

	@GET
	@Path("/{idEpreuve}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<QuestionTirage> getQuestionTirage(@PathParam("idEpreuve") int idEpr) {
		List<QuestionTirage> resultat = new ArrayList<QuestionTirage>();
		resultat = qtDAO.selectByEpreuveId(idEpr);

		return resultat;

	}
	

	@POST
	@Path("/inscriptionepreuve")
	@Consumes(MediaType.APPLICATION_JSON)
	public JsonInscription save(JsonInscription jsonIns) {		
		Date date1 = new Date();
		Date date2 = new Date();
		try {
			 date1 = sdf.parse(jsonIns.getDateDebutValidite().toString());
			 date2 = sdf.parse(jsonIns.getDateFinValidite().toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(date1.compareTo(date2) >= 0) {
			return null;
		}else if(!jsonIns.isValid()){
			return null;
		}
		EpreuveManager epreuveManager = new EpreuveManager();
		epreuveManager.insertEpreuve(jsonIns);
		
		return null;
	}

	@POST
	@Path("/candidatinscrit")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Epreuve> setReponseIns(@FormParam("data") String jsonCandidatIns) {
		List<Epreuve> listEpreuve = new ArrayList<Epreuve>();
		Map<String, String> map = new Gson().fromJson(jsonCandidatIns, new TypeToken<HashMap<String, String>>(){}.getType());
				System.out.println(Integer.parseInt(map.get("idtest")));
			EpreuveManager epreuveManager = new EpreuveManager();
			listEpreuve = epreuveManager.selectAllByIdTest(Integer.parseInt(map.get("idtest")));
			Gson gson = new Gson();

			// 1. Java object to JSON, and save into a file
			try {
				gson.toJson(listUtilisateur, new FileWriter("D:\\file.json"));
			} catch (JsonIOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(listUtilisateur);

			return listEpreuve;

	}
}
