package fr.eni.projetqcm.dal;

import java.util.List;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Utilisateur;

public interface EpreuveDAO {

	public void insert(Epreuve epreuve);
	public void update(Epreuve epreuve);
	public void delete(int id);
	public List<Epreuve> selectAll();
	public List<Epreuve> selectAllByIdTest(int idTest);
	public List<Epreuve> selectAllByUserIdAp(Utilisateur utilisateur);
	public List<Epreuve> selectAllByUserIdT(Utilisateur utilisateur);
	public Epreuve selectById(int id);
	
}
