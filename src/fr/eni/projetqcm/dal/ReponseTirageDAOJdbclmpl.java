package fr.eni.projetqcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Proposition;
import fr.eni.projetqcm.bo.Question;
import fr.eni.projetqcm.bo.QuestionTirage;
import fr.eni.projetqcm.bo.ReponseTirage;
import fr.eni.projetqcm.bo.SectionTest;
import fr.eni.projetqcm.bo.Test;
import fr.eni.projetqcm.bo.Theme;

public class ReponseTirageDAOJdbclmpl implements ReponseTirageDAO {

	private static final String SELECT_ALL = "SELECT idTest, libelle, description, duree, seuil_haut, seuil_bas"
			+ "FROM Test";
	private static final String SELECT_BY_EPREUVE_ID = "SELECT qt.estMarquee, qt.numOrdre, "
			+ "q.idQuestion, q.enonce as enonceQuestion, q.media, q.points, q.idTheme, "
			+ "p.idProposition, p.enonce as enonceProposition, p.estBonne, "
			+ "e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule, e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur, "
			+ "t.idTest as idTest, t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas, "
			+ "st.nbQuestionsATirer, " 
			+ "tm.libelle as libelleTheme, tm.idTheme as idTheme "
			+ "FROM REPONSE_TIRAGE as rt " 
			+ "INNER JOIN QUESTION_TIRAGE as qt ON rt.idQuestion = qt.idQuestion "
			+ "INNER JOIN QUESTION as q ON qt.idQuestion = q.idQuestion "
			+ "INNER JOIN PROPOSITION as p ON rt.idProposition = p.idProposition "
			+ "INNER JOIN EPREUVE as e ON qt.idEpreuve = e.idEpreuve " 
			+ "INNER JOIN TEST as t ON e.idTest = t.idTest "
			+ "INNER JOIN SECTION_TEST as st ON t.idTest = st.idTest "
			+ "INNER JOIN THEME as tm ON st.idTheme = tm.idTheme "
			+ "WHERE rt.idEpreuve = ?";
	private static final String SELECT_BY_QUESTION = "SELECT qt.estMarquee, qt.numOrdre, "
			+ "q.idQuestion, q.enonce as enonceQuestion, q.media, q.points, q.idTheme, "
			+ "p.idProposition, p.enonce as enonceProposition, p.estBonne, "
			+ "e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule, e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur, "
			+ "t.idTest as idTest, t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas, "
			+ "st.nbQuestionsATirer, " 
			+ "tm.libelle as libelleTheme, tm.idTheme as idTheme "
			+ "FROM REPONSE_TIRAGE as rt " 
			+ "INNER JOIN QUESTION_TIRAGE as qt ON rt.idQuestion = qt.idQuestion "
			+ "INNER JOIN QUESTION as q ON qt.idQuestion = q.idQuestion "
			+ "INNER JOIN PROPOSITION as p ON rt.idProposition = p.idProposition "
			+ "INNER JOIN EPREUVE as e ON qt.idEpreuve = e.idEpreuve " 
			+ "INNER JOIN TEST as t ON e.idTest = t.idTest "
			+ "INNER JOIN SECTION_TEST as st ON t.idTest = st.idTest "
			+ "INNER JOIN THEME as tm ON st.idTheme = tm.idTheme "
			+ "WHERE rt.idEpreuve = ? AND rt.idQuestion = ?";
	private static final String SELECT_BY_REPONSE = "SELECT qt.estMarquee, qt.numOrdre, "
			+ "q.idQuestion, q.enonce as enonceQuestion, q.media, q.points, q.idTheme, "
			+ "p.idProposition, p.enonce as enonceProposition, p.estBonne, "
			+ "e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule, e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur, "
			+ "t.idTest as idTest, t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas, "
			+ "st.nbQuestionsATirer, " + "tm.libelle as libelleTheme, tm.idTheme as idTheme "
			+ "FROM REPONSE_TIRAGE as rt " + "INNER JOIN QUESTION_TIRAGE as qt ON rt.idQuestion = qt.idQuestion "
			+ "INNER JOIN QUESTION as q ON qt.idQuestion = q.idQuestion "
			+ "INNER JOIN PROPOSITION as p ON rt.idProposition = p.idProposition "
			+ "INNER JOIN EPREUVE as e ON qt.idEpreuve = e.idEpreuve " + "INNER JOIN TEST as t ON e.idTest = t.idTest "
			+ "INNER JOIN SECTION_TEST as st ON t.idTest = st.idTest "
			+ "INNER JOIN THEME as tm ON st.idTheme = tm.idTheme "
			+ "WHERE rt.idProposition = ? AND rt.idQuestion = ? AND rt.idEpreuve = ?";
	private static final String INSERT_TEST = "INSERT INTO REPONSE_TIRAGE VALUES (?, ?, ?)";
	private static final String DELETE_REPONSE_TIRAGE = "DELETE FROM REPONSE_TIRAGE WHERE idProposition = ? AND idQuestion = ? AND idEpreuve = ?";

	@Override
	public void insert(ReponseTirage reponseTirage) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_TEST);
			pstmt.setInt(1, reponseTirage.getProposition().getIdProposition());
			pstmt.setInt(2, reponseTirage.getQuestionTirage().getQuestion().getIdQuestion());
			pstmt.setInt(3, reponseTirage.getEpreuve().getIdEpreuve());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

	}

	@Override
	public void delete(ReponseTirage reponseTirage) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_REPONSE_TIRAGE);
			pstmt.setInt(1, reponseTirage.getProposition().getIdProposition());
			pstmt.setInt(2, reponseTirage.getQuestionTirage().getQuestion().getIdQuestion());
			pstmt.setInt(3, reponseTirage.getEpreuve().getIdEpreuve());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
	}

	@Override
	public List<ReponseTirage> selectAll() {

		List<ReponseTirage> ListReponseTirage = new ArrayList<ReponseTirage>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
//				ListQuestionTirage.add(new Test(rs.getInt("idTest"), rs.getString("libelle"), rs.getString("description"),
//						rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas")));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return ListReponseTirage;
	}
	
	@Override
	public List<ReponseTirage> selectByQuestion(ReponseTirage reponseTirage) {
		List<Integer> listIdTheme = null;
		List<ReponseTirage> list = new ArrayList<ReponseTirage>();
		ReponseTirage rt = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_QUESTION);
			pstmt.setInt(1, reponseTirage.getEpreuve().getIdEpreuve());
			pstmt.setInt(2, reponseTirage.getQuestionTirage().getQuestion().getIdQuestion());
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne || rt.getProposition().getIdProposition() != rs.getInt("idProposition")) {
					rt = new ReponseTirage();
					rt.setQuestionTirage(new QuestionTirage(rs.getBoolean("estMarquee"),
							new Question(rs.getInt("idQuestion"), rs.getString("enonceQuestion"), rs.getString("media"),
									rs.getInt("points"), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme")),
									new ArrayList<Proposition>()),
							rs.getInt("numOrdre"),
							new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
									rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
									rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
									new Test(rs.getInt("idTest"), rs.getString("libelleTest"),
											rs.getString("description"), rs.getInt("duree"), rs.getInt("seuil_haut"),
											rs.getInt("seuil_bas"), new ArrayList<SectionTest>()),
									rs.getInt("idUtilisateur"))));
					rt.setProposition(new Proposition(rs.getInt("idProposition"), rs.getString("enonceProposition"),
							rs.getBoolean("estBonne"), rt.getQuestionTirage().getQuestion()));
					rt.setEpreuve(rt.getQuestionTirage().getEpreuve());
					
					listIdTheme = new ArrayList<Integer>();
					list.add(rt);
					premiereligne = false;
				}
				if (!listIdTheme.contains(rs.getInt("idTheme"))) {
					listIdTheme.add(rs.getInt("idTheme"));
					rt.getEpreuve().getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"),
							rt.getEpreuve().getTest(), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return list;
	}

	@Override
	public List<ReponseTirage> selectByEpreuveId(int id) {
		List<Integer> listIdTheme = null;
		List<ReponseTirage> list = new ArrayList<ReponseTirage>();
		ReponseTirage rt = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_EPREUVE_ID);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne || rt.getProposition().getIdProposition() != rs.getInt("idProposition")) {
					rt = new ReponseTirage();
					rt.setQuestionTirage(new QuestionTirage(rs.getBoolean("estMarquee"),
							new Question(rs.getInt("idQuestion"), rs.getString("enonceQuestion"), rs.getString("media"),
									rs.getInt("points"), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme")),
									new ArrayList<Proposition>()),
							rs.getInt("numOrdre"),
							new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
									rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
									rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
									new Test(rs.getInt("idTest"), rs.getString("libelleTest"),
											rs.getString("description"), rs.getInt("duree"), rs.getInt("seuil_haut"),
											rs.getInt("seuil_bas"), new ArrayList<SectionTest>()),
									rs.getInt("idUtilisateur"))));
					rt.setProposition(new Proposition(rs.getInt("idProposition"), rs.getString("enonceProposition"),
							rs.getBoolean("estBonne"), rt.getQuestionTirage().getQuestion()));
					rt.setEpreuve(rt.getQuestionTirage().getEpreuve());
					
					listIdTheme = new ArrayList<Integer>();
					list.add(rt);
					premiereligne = false;
				}
				if (!listIdTheme.contains(rs.getInt("idTheme"))) {
					listIdTheme.add(rs.getInt("idTheme"));
					rt.getEpreuve().getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"),
							rt.getEpreuve().getTest(), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return list;
	}

	@Override
	public ReponseTirage selectByReponse(ReponseTirage reponseTirage) {
		List<Integer> listIdTheme = null;
		ReponseTirage rt = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_REPONSE);
			pstmt.setInt(1, reponseTirage.getProposition().getIdProposition());
			pstmt.setInt(2, reponseTirage.getQuestionTirage().getQuestion().getIdQuestion());
			pstmt.setInt(3, reponseTirage.getEpreuve().getIdEpreuve());
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne) {
					rt = new ReponseTirage();
					rt.setQuestionTirage(new QuestionTirage(rs.getBoolean("estMarquee"),
							new Question(rs.getInt("idQuestion"), rs.getString("enonceQuestion"), rs.getString("media"),
									rs.getInt("points"), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme")),
									new ArrayList<Proposition>()),
							rs.getInt("numOrdre"),
							new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
									rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
									rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
									new Test(rs.getInt("idTest"), rs.getString("libelleTest"),
											rs.getString("description"), rs.getInt("duree"), rs.getInt("seuil_haut"),
											rs.getInt("seuil_bas"), new ArrayList<SectionTest>()),
									rs.getInt("idUtilisateur"))));
					rt.setProposition(new Proposition(rs.getInt("idProposition"), rs.getString("enonceProposition"),
							rs.getBoolean("estBonne"), rt.getQuestionTirage().getQuestion()));
					rt.setEpreuve(rt.getQuestionTirage().getEpreuve());

					listIdTheme = new ArrayList<Integer>();
					premiereligne = false;
				}
				if (!listIdTheme.contains(rs.getInt("idTheme"))) {
					listIdTheme.add(rs.getInt("idTheme"));
					rt.getEpreuve().getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"),
							rt.getEpreuve().getTest(), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return rt;
	}

}
