package fr.eni.projetqcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.SectionTest;
import fr.eni.projetqcm.bo.Test;
import fr.eni.projetqcm.bo.Theme;

public class TestDAOJdbclmpl implements TestDAO {

	private static final String SELECT_ALL = "SELECT t.idTest, t.libelle, t.description, t.duree, t.seuil_haut, t.seuil_bas, st.nbQuestionsATirer, tm.idTheme, tm.libelle" + 
			" FROM Test t" + 
			" INNER JOIN SECTION_TEST st ON st.idTest = t.idTest" + 
			" INNER JOIN THEME tm ON tm.idTheme = st.idTheme";
	private static final String SELECT_BY_ID = "SELECT t.idTest, t.libelle, t.description, t.duree, t.seuil_haut, t.seuil_bas, st.nbQuestionsATirer, tm.idTheme, tm.libelle" + 
			" FROM Test t" + 
			" INNER JOIN SECTION_TEST st ON st.idTest = t.idTest" + 
			" INNER JOIN THEME tm ON tm.idTheme = st.idTheme" + 
			" WHERE t.idTest = ?";
	private static final String INSERT_TEST = "INSERT INTO Test (idTest, libelle, description, duree, seuil_haut, seuil_bas"
			+ " VALUES (?, ?, ?, ?, ?, ?);";
	private static final String DELETE_TEST = "DELETE FROM Test WHERE idTest=?";
	private static final String UPDATE_TEST = "DELETE FROM Test WHERE idTest=?";

	@Override
	public void insert(Test test) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_TEST);
			pstmt.setInt(1, test.getIdTest());
			pstmt.setString(2, test.getLibelle());
			pstmt.setString(3, test.getDescription());
			pstmt.setInt(4, test.getDuree());
			pstmt.setInt(5, test.getSeuil_haut());
			pstmt.setInt(6, test.getSeuil_bas());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

	}

	@Override
	public void update(Test test) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		Test test = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_TEST);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
	}

	@Override
	public List<Test> selectAll() {
		List<Test> ListTest = new ArrayList<Test>();
		List<SectionTest> ListSection = new ArrayList<SectionTest>();
		Test test = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne || rs.getInt("idTest") != test.getIdTest()) {
					test = new Test(rs.getInt("idTest"), rs.getString("libelle"), rs.getString("description"),
							rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas"), ListSection);
					ListTest.add(test);
					premiereligne = false;
				}
				ListSection.add(new SectionTest(rs.getInt("nbQuestionsATirer"), test,
						new Theme(rs.getInt("idTheme"), rs.getString("libelle"))));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
		return ListTest;
	}

	@Override
	public Test selectById(int id) {
		List<SectionTest> ListSection = new ArrayList<SectionTest>();
		Test test = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_ID);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne || rs.getInt("idTest") != test.getIdTest()) {
					test = new Test(rs.getInt("idTest"), rs.getString("libelle"), rs.getString("description"),
							rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas"), ListSection);
					premiereligne = false;
				}
				ListSection.add(new SectionTest(rs.getInt("nbQuestionsATirer"), test,
						new Theme(rs.getInt("idTheme"), rs.getString("libelle"))));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return test;
	}

}
