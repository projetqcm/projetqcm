package fr.eni.projetqcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.Promotion;


public class PromotionDAOJdbcImpl implements PromotionDAO {
	
	private static final String SELECT_ALL = "SELECT codePromo, libelle"
			+ " FROM Promotion";
	
	private static final String SELECT_ALL_BY_MASQUE = "SELECT codePromo, libelle"
			+ " FROM Promotion"
			+ " WHERE codePromo LIKE ?";
	
	@Override
	public void insert(Promotion promotion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Promotion promotion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Promotion> selectAll() {
		List<Promotion> listPromotion = new ArrayList<Promotion>();
		
		try (Connection cnx = ConnectionProvider.getConnection()) {
			
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				listPromotion.add(new Promotion(rs.getString("codePromo"), rs.getString("libelle")));
			}

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return listPromotion;
	}

	@Override
	public Promotion selectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Promotion> selectPromotionByMasque(String promotion) {
		List<Promotion> listPromotion = new ArrayList<Promotion>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_BY_MASQUE);
			pstmt.setString(1, "%"+promotion+"%");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				listPromotion.add(new Promotion(rs.getString("codePromo"), rs.getString("libelle")));
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		return listPromotion;
	}

}
