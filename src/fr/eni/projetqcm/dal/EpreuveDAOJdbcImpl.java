package fr.eni.projetqcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.SectionTest;
import fr.eni.projetqcm.bo.Test;
import fr.eni.projetqcm.bo.Theme;
import fr.eni.projetqcm.bo.Utilisateur;

public class EpreuveDAOJdbcImpl implements EpreuveDAO {

	private static final String SELECT_ALL = "SELECT e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule,"
			+ " e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur, t.idTest as idTest,"
			+ " t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas,"
			+ " st.nbQuestionsATirer, tm.libelle as libelleTheme, tm.idTheme as idTheme" + " FROM EPREUVE e"
			+ " INNER JOIN TEST t on t.idTest = e.idTest" + " INNER JOIN SECTION_TEST st on st.idTest = t.idTest"
			+ " INNER JOIN THEME tm on tm.idTheme = st.idTheme" 
			+ " WHERE e.etat = 'EA'";
	
	private static final String SELECT_ALL_BY_IDTEST = "SELECT e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule,"
			+ " e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur as eprIdUtilisateur, t.idTest as idTest," 
			+ " t.libelle, t.description, t.duree, t.seuil_haut, t.seuil_bas, u.idUtilisateur as utiIdUtilisateur, u.prenom, u.nom, u.email," 
			+ " u.codePromo, u.codeProfil"
			+ " FROM EPREUVE e"
			+ " INNER JOIN TEST t on t.idTest = e.idTest" 
			+ " INNER JOIN UTILISATEUR u ON u.idUtilisateur = e.idUtilisateur"
			+ " WHERE e.etat = 'EA' AND t.idTest = ? AND u.codeProfil <> 1";
	
	private static final String SELECT_ALL_BY_USER_ID_AP = "SELECT e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule,"
			+ " e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur, t.idTest as idTest,"
			+ " t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas,"
			+ " st.nbQuestionsATirer, tm.libelle as libelleTheme, tm.idTheme as idTheme" + " FROM EPREUVE e"
			+ " INNER JOIN TEST t on t.idTest = e.idTest" + " INNER JOIN SECTION_TEST st on st.idTest = t.idTest"
			+ " INNER JOIN THEME tm on tm.idTheme = st.idTheme" + " WHERE e.idUtilisateur = ? AND e.etat <> 'T' AND e.dateFinValidite >= GETDATE()";
	
	private static final String SELECT_ALL_BY_USER_ID_T = "SELECT e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule,"
			+ " e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur, t.idTest as idTest,"
			+ " t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas,"
			+ " st.nbQuestionsATirer, tm.libelle as libelleTheme, tm.idTheme as idTheme" + " FROM EPREUVE e"
			+ " INNER JOIN TEST t on t.idTest = e.idTest" + " INNER JOIN SECTION_TEST st on st.idTest = t.idTest"
			+ " INNER JOIN THEME tm on tm.idTheme = st.idTheme" + " WHERE e.idUtilisateur = ? AND e.etat = 'T'";
	
	private static final String SELECT_BY_ID = "SELECT e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule,"
			+ " e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur, t.idTest as idTest,"
			+ " t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas,"
			+ " st.nbQuestionsATirer, tm.libelle as libelleTheme, tm.idTheme as idTheme" + " FROM EPREUVE e "
			+ "INNER JOIN TEST t on t.idTest = e.idTest " + "INNER JOIN SECTION_TEST st on st.idTest = t.idTest "
			+ "INNER JOIN THEME tm on tm.idTheme = st.idTheme " + "WHERE e.idEpreuve = ?";
	
	private static final String INSERT_EPREUVE = "INSERT INTO Epreuve (dateDebutValidite, dateFinValidite, tempsEcoule, etat, note_obtenue, niveau_obtenu,"
			+ " idTest, idUtilisateur)"
			+ " VALUES (?, ?, ?, ?, null, null, ?, ?)";
	
	private static final String DELETE_EPREUVE = "DELETE FROM Epreuve WHERE idEpreuve=?";
	private static final String UPDATE_EPREUVE = "UPDATE Epreuve SET dateDebutValidite = ?, dateFinValidite = ?, tempsEcoule = ?, etat = ?, note_obtenue = ?, niveau_obtenu = ? WHERE idEpreuve = ?";

	@Override
	public void insert(Epreuve epreuve) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_EPREUVE);

			pstmt.setDate(1, new java.sql.Date(epreuve.getdateDebutValidite().getTime()));
			pstmt.setDate(2, new java.sql.Date(epreuve.getDateFinValidite().getTime()));
			pstmt.setInt(3, epreuve.getTempsEcoule());
			pstmt.setString(4, epreuve.getEtat());
			pstmt.setInt(5, epreuve.getTest().getIdTest());
			pstmt.setInt(6, epreuve.getIdUtilisateur());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

	}

	@Override
	public void update(Epreuve epreuve) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_EPREUVE);
			pstmt.setDate(1, new java.sql.Date(epreuve.getdateDebutValidite().getTime()));
			pstmt.setDate(2, new java.sql.Date(epreuve.getDateFinValidite().getTime()));
			pstmt.setInt(3, epreuve.getTempsEcoule());
			pstmt.setString(4, epreuve.getEtat());
			pstmt.setFloat(5, epreuve.getNote_obtenue());
			pstmt.setString(6, epreuve.getNiveau_obtenu());
			pstmt.setInt(7, epreuve.getIdEpreuve());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

	}

	@Override
	public void delete(int id) {

	}

	@Override
	public List<Epreuve> selectAll() {
		List<Epreuve> ListEpreuve = new ArrayList<Epreuve>();
		Epreuve epreuve = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = pstmt.executeQuery();
			Boolean premierligne = true;
			while (rs.next()) {

				if (premierligne || rs.getInt("idEpreuve") != epreuve.getIdEpreuve()) {
					epreuve = new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
							rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
							rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
							new Test(rs.getInt("idTest"), rs.getString("libelleTest"), rs.getString("description"),
									rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas"),
									new ArrayList<SectionTest>()),
							rs.getInt("idUtilisateur"));
					ListEpreuve.add(epreuve);

					premierligne = false;
				}
				epreuve.getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"), epreuve.getTest(),
						new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
			}

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return ListEpreuve;
	}

	@Override
	public List<Epreuve> selectAllByUserIdAp(Utilisateur utilisateur) {
		List<Epreuve> ListEpreuve = new ArrayList<Epreuve>();
		Epreuve epreuve = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_BY_USER_ID_AP);
			pstmt.setInt(1, utilisateur.getId());
			ResultSet rs = pstmt.executeQuery();
			Boolean premierligne = true;
			while (rs.next()) {

				if (premierligne || rs.getInt("idEpreuve") != epreuve.getIdEpreuve()) {
					epreuve = new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
							rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
							rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
							new Test(rs.getInt("idTest"), rs.getString("libelleTest"), rs.getString("description"),
									rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas"),
									new ArrayList<SectionTest>()),
							rs.getInt("idUtilisateur"));
					ListEpreuve.add(epreuve);

					premierligne = false;
				}
				epreuve.getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"), epreuve.getTest(),
						new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
			}

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return ListEpreuve;
	}

	@Override
	public List<Epreuve> selectAllByUserIdT(Utilisateur utilisateur) {
		List<Epreuve> ListEpreuve = new ArrayList<Epreuve>();
		Epreuve epreuve = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_BY_USER_ID_T);
			pstmt.setInt(1, utilisateur.getId());
			ResultSet rs = pstmt.executeQuery();
			Boolean premierligne = true;
			while (rs.next()) {

				if (premierligne || rs.getInt("idEpreuve") != epreuve.getIdEpreuve()) {
					epreuve = new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
							rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
							rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
							new Test(rs.getInt("idTest"), rs.getString("libelleTest"), rs.getString("description"),
									rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas"),
									new ArrayList<SectionTest>()),
							rs.getInt("idUtilisateur"));
					ListEpreuve.add(epreuve);

					premierligne = false;
				}
				epreuve.getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"), epreuve.getTest(),
						new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
			}

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return ListEpreuve;
	}

	@Override
	public Epreuve selectById(int id) {
		Epreuve epreuve = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_ID);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			Boolean premierligne = true;
			while (rs.next()) {
				if (premierligne) {
					epreuve = new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
							rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
							rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
							new Test(rs.getInt("idTest"), rs.getString("libelleTest"), rs.getString("description"),
									rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas"),
									new ArrayList<SectionTest>()),
							rs.getInt("idUtilisateur"));
					premierligne = false;
				}
				epreuve.getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"), epreuve.getTest(),
						new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
			}

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return epreuve;
	}

	@Override
	public List<Epreuve> selectAllByIdTest(int idTest) {
		List<Epreuve> ListEpreuve = new ArrayList<Epreuve>();
		Epreuve epreuve = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_BY_IDTEST);
			pstmt.setInt(1, idTest);
			ResultSet rs = pstmt.executeQuery();
			Boolean premierligne = true;
			while (rs.next()) {

				if (premierligne || rs.getInt("idEpreuve") != epreuve.getIdEpreuve()) {
					epreuve = new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
							rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
							rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"), 
							new Utilisateur(rs.getInt("utiIdUtilisateur"), rs.getString("prenom"), rs.getString("nom"), rs.getString("email")));
					ListEpreuve.add(epreuve);

					premierligne = false;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return ListEpreuve;
	}





}
