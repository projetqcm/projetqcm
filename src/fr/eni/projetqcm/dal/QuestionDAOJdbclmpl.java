package fr.eni.projetqcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.Proposition;
import fr.eni.projetqcm.bo.Question;
import fr.eni.projetqcm.bo.Theme;

public class QuestionDAOJdbclmpl implements QuestionDAO {

	private static final String SELECT_ALL = "SELECT q.idQuestion, " 
			+ "q.enonce as enonceQuestion, q.media, q.points, " 
			+ "q.idTheme, p.enonce as enonceProposition, p.estBonne, " 
			+ "p.idProposition as idProposition, t.libelle as libelleTheme " 
			+ "FROM QUESTION q " 
			+ "INNER JOIN PROPOSITION p ON p.idQuestion = q.idQuestion " 
			+ "INNER JOIN THEME t ON t.idTheme = q.idTheme";
	private static final String SELECT_ALL_BY_THEME = "SELECT q.idQuestion, " 
			+ "q.enonce as enonceQuestion, q.media, q.points, " 
			+ "q.idTheme, p.enonce as enonceProposition, p.estBonne, " 
			+ "p.idProposition as idProposition, t.libelle as libelleTheme " 
			+ "FROM QUESTION q "
			+ "INNER JOIN PROPOSITION p ON p.idQuestion = q.idQuestion " 
			+ "INNER JOIN THEME t ON t.idTheme = q.idTheme "
			+ "WHERE q.idTheme = ?";
	private static final String SELECT_BY_ID = "SELECT idQuestion, enonce, media, points, idTheme"
			+ "FROM QUESTION WHERE id = ?";
	private static final String INSERT_QUESTION = "INSERT INTO QUESTION (enonce, media, points, idTheme)"
			+ "VALUES (?, ?, ?, ?, ?, ?);";
	private static final String DELETE_QUESTION = "DELETE FROM QUESTION WHERE idQuestion=?";
	private static final String UPDATE_QUESTION = "DELETE FROM QUESTION WHERE idQuestion=?";

	@Override
	public void insert(Question question) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_QUESTION);
			pstmt.setString(1, question.getEnonce());
			pstmt.setString(2, question.getMedia());
			pstmt.setInt(3, question.getPoints());
			pstmt.setInt(4, question.getTheme().getIdTheme());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

	}

	@Override
	public void update(Question question) {

	}

	@Override
	public void delete(int id) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_QUESTION);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
	}

	@Override
	public List<Question> selectAll() {
		List<Question> list = new ArrayList<Question>();
		Question question = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = pstmt.executeQuery();
			boolean premierligne = true;
			while (rs.next()) {
				if (premierligne || rs.getInt("idQuestion") != question.getIdQuestion()) {
					question = new Question(rs.getInt("idQuestion"), rs.getString("enonceQuestion"), rs.getString("media"),
							rs.getInt("points"), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme")), new ArrayList<Proposition>());
					list.add(question);
				}
				question.addProposition(new Proposition(rs.getInt("idProposition"), rs.getString("enonceProposition"), rs.getBoolean("estBonne"), question));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return list;
	}
	
	@Override
	public List<Question> selectAllByTheme(Theme theme) {
		List<Question> list = new ArrayList<Question>();
		Question question = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_BY_THEME);
			pstmt.setInt(1, theme.getIdTheme());
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne || rs.getInt("idQuestion") != question.getIdQuestion()) {
					question = new Question(rs.getInt("idQuestion"), rs.getString("enonceQuestion"), rs.getString("media"),
							rs.getInt("points"), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme")), new ArrayList<Proposition>());
					list.add(question);
					premiereligne = false;
				}
				question.addProposition(new Proposition(rs.getInt("idProposition"), rs.getString("enonceProposition"), rs.getBoolean("estBonne"), question));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return list;
	}

	@Override
	public Question selectById(int id) {
		Question question = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_ID);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
//				questionTirage = new Test(rs.getInt("idTest"), rs.getString("libelle"), rs.getString("description"),
//						rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas"));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return question;
	}

}
