package fr.eni.projetqcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import fr.eni.projetqcm.bo.Profil;
import fr.eni.projetqcm.bo.Promotion;
import fr.eni.projetqcm.bo.Utilisateur;
import fr.eni.projetqcm.utils.PasswordUtils;

public class UtilisateurDAOJdbcImpl implements UtilisateurDAO {

	private static final String SELECT_ALL = "SELECT idUtilisateur, nom, prenom, email, "
			+ "password, profil.codeProfil as codeProfil, profil.libelle as libelleProfil, "
			+ "promo.codePromo as codePromo, promo.libelle as libellePromo, salt " 
			+ "FROM UTILISATEUR as u "
			+ "LEFT JOIN PROMOTION as promo ON promo.codePromo = u.codePromo "
			+ "LEFT JOIN PROFIL as profil ON profil.codeProfil = u.codeProfil";
	private static final String SELECT_BY_ID = "SELECT idUtilisateur, nom, prenom, email, "
			+ "password, profil.codeProfil as codeProfil, profil.libelle as libelleProfil, "
			+ "promo.codePromo as codePromo, promo.libelle as libellePromo, salt " 
			+ "FROM UTILISATEUR as u "
			+ "LEFT JOIN PROMOTION as promo ON promo.codePromo = u.codePromo "
			+ "LEFT JOIN PROFIL as profil ON profil.codeProfil = u.codeProfil " 
			+ "WHERE id = ?";
	private static final String SELECT_BY_EMAIL = "SELECT idUtilisateur, nom, prenom, email, "
			+ "password, profil.codeProfil as codeProfil, profil.libelle as libelleProfil, "
			+ "promo.codePromo as codePromo, promo.libelle as libellePromo, salt " 
			+ "FROM UTILISATEUR as u "
			+ "LEFT JOIN PROMOTION as promo ON promo.codePromo = u.codePromo "
			+ "LEFT JOIN PROFIL as profil ON profil.codeProfil = u.codeProfil " 
			+ "WHERE email = ?";
	private static final String SELECT_BY_MASQUE = "SELECT * FROM UTILISATEUR WHERE ( nom LIKE ? OR prenom LIKE ?)" + 
			" AND idUtilisateur NOT IN(SELECT u.idUtilisateur FROM UTILISATEUR u" + 
			" INNER JOIN profil p on u.codeProfil = p.codeProfil" + 
			" LEFT JOIN EPREUVE e on u.idUtilisateur = e.idUtilisateur" + 
			" WHERE e.idTest = ? AND p.codeProfil <> 1)";
	private static final String SELECT_BY_ID_TEST = "SELECT idUtilisateur, nom, prenom, email, "
			+ "password, profil.codeProfil as codeProfil, profil.libelle as libelleProfil, "
			+ "promo.codePromo as codePromo, promo.libelle as libellePromo, salt " 
			+ "FROM UTILISATEUR as u "
			+ "LEFT JOIN PROMOTION as promo ON promo.codePromo = u.codePromo "
			+ "LEFT JOIN PROFIL as profil ON profil.codeProfil = u.codeProfil " 
			+ "WHERE id = ?";
	private static final String INSERT_UTILISATEUR = "INSERT INTO UTILISATEUR (nom, prenom, email, "
			+ "password, codeProfil, codePromo, salt) VALUES (?, ?, ?, ?, ?, ?, ?);";
	private static final String DELETE_UTILISATEUR = "DELETE FROM UTILISATEUR WHERE idUtilisateur=?";

	@Override
	public void insert(Utilisateur utilisateur){

		try (Connection cnx = ConnectionProvider.getConnection()) {
			try {
				String salt = PasswordUtils.getSalt(30);
				cnx.setAutoCommit(false);
				PreparedStatement pstmt;
				ResultSet rs;

				pstmt = cnx.prepareStatement(INSERT_UTILISATEUR, PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, utilisateur.getNom());
				pstmt.setString(2, utilisateur.getPrenom());
				pstmt.setString(3, utilisateur.getEmail());
				pstmt.setString(4, PasswordUtils.generateSecurePassword(utilisateur.getPassword(), salt));
				pstmt.setInt(5, utilisateur.getProfil().getCodeProfil());
				pstmt.setString(6, utilisateur.getPromotion().getCodePromo());
				pstmt.setString(7, salt);
				pstmt.executeUpdate();
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					utilisateur.setId(rs.getInt(1));
				}
				rs.close();
				pstmt.close();
				
				cnx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				cnx.rollback();
				throw e;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Override
	public void update(Utilisateur utilisateur) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_UTILISATEUR);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();

		} catch (SQLException sqle) {
			sqle.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.SUPPRESSION_LISTE_ERREUR);
//			throw businessException;
		}
	}

	@Override
	public List<Utilisateur> selectAll() {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				utilisateurs.add(new Utilisateur(rs.getInt("idUtilisateur"), rs.getString("nom"),
						rs.getString("prenom"), rs.getString("email"), rs.getString("password"),
						new Profil(rs.getInt("codeProfil"), rs.getString("libelleProfil")),
						new Promotion(rs.getString("codePromo"), rs.getString("libellePromo")), rs.getString("salt")));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
		return utilisateurs;
	}

	@Override
	public Utilisateur selectById(int id) {
		Utilisateur utilisateur = null;
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_ID);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				utilisateur = new Utilisateur(rs.getInt("idUtilisateur"), rs.getString("nom"), rs.getString("prenom"),
						rs.getString("email"), rs.getString("password"),
						new Profil(rs.getInt("codeProfil"), rs.getString("libelleProfil")),
						new Promotion(rs.getString("codePromo"), rs.getString("libellePromo")), rs.getString("salt"));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTE_ECHEC);
//			throw businessException;
		}
//		if(user.getId()==0)
//		{
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTE_INEXISTANTE);
//			throw businessException;
//		}

		return utilisateur;
	}

	@Override
	public Utilisateur selectByEmail(String email) {
		Utilisateur utilisateur = null;
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_EMAIL);
			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				utilisateur = new Utilisateur(rs.getInt("idUtilisateur"), rs.getString("nom"), rs.getString("prenom"),
						rs.getString("email"), rs.getString("password"),
						new Profil(rs.getInt("codeProfil"), rs.getString("libelleProfil")),
						new Promotion(rs.getString("codePromo"), rs.getString("libellePromo")), rs.getString("salt"));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTE_ECHEC);
//			throw businessException;
		}
//		if(user.getId()==0)
//		{
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTE_INEXISTANTE);
//			throw businessException;
//		}

		return utilisateur;
	}

	@Override
	public List<Utilisateur> selectByMasque(Utilisateur utilisateur, int idTest) {
		List<Utilisateur> listUtilisateur = new ArrayList<Utilisateur>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_MASQUE);
			pstmt.setString(1, "%"+utilisateur.getPrenom()+"%");
			pstmt.setString(2, "%"+utilisateur.getNom()+"%");
			pstmt.setInt(3, idTest);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				utilisateur = new Utilisateur(rs.getInt("idUtilisateur"), rs.getString("nom"), rs.getString("prenom"),
						rs.getString("email"));
				listUtilisateur.add(utilisateur);
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return listUtilisateur;
	}

	@Override
	public List<Utilisateur> selectByIdTest(Utilisateur utilisateur, int idTest) {
		List<Utilisateur> listUtilisateur = new ArrayList<Utilisateur>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_ID_TEST);
			pstmt.setInt(3, idTest);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				utilisateur = new Utilisateur(rs.getInt("idUtilisateur"), rs.getString("nom"), rs.getString("prenom"),
						rs.getString("email"));
				listUtilisateur.add(utilisateur);
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return listUtilisateur;
	}
}
