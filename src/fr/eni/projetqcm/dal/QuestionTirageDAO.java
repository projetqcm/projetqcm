package fr.eni.projetqcm.dal;

import java.util.List;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.QuestionTirage;

public interface QuestionTirageDAO {
	public void insert(QuestionTirage qt);
	public void delete(int id);
	public void deleteByEpreuve(Epreuve epr);
	public void update(QuestionTirage qt);
	public QuestionTirage selectByQuestion(QuestionTirage qt);
	public List<QuestionTirage> selectAll();
	public List<QuestionTirage> selectByEpreuveId(int id);
}
