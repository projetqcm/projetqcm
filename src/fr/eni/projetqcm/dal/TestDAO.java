package fr.eni.projetqcm.dal;

import java.util.List;

import fr.eni.projetqcm.bo.Test;

public interface TestDAO {
	public void insert(Test test);
	public void update(Test test);
	public void delete(int id);
	public List<Test> selectAll();
	public Test selectById(int id);
}
