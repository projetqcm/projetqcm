package fr.eni.projetqcm.dal;

import java.util.List;

import fr.eni.projetqcm.bo.Question;
import fr.eni.projetqcm.bo.Theme;

public interface QuestionDAO {
	public void insert(Question question);
	public void update(Question question);
	public void delete(int id);
	public List<Question> selectAll();
	public List<Question> selectAllByTheme(Theme theme);
	public Question selectById(int id);
}
