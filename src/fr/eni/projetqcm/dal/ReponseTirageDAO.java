package fr.eni.projetqcm.dal;

import java.util.List;

import fr.eni.projetqcm.bo.QuestionTirage;
import fr.eni.projetqcm.bo.ReponseTirage;

public interface ReponseTirageDAO {
	public void insert(ReponseTirage reponseTirage);
	public void delete(ReponseTirage reponseTirage);
	public List<ReponseTirage> selectAll();
	public List<ReponseTirage> selectByEpreuveId(int id);
	public ReponseTirage selectByReponse(ReponseTirage reponseTirage);
	public List<ReponseTirage> selectByQuestion(ReponseTirage reponseTirage);
}
