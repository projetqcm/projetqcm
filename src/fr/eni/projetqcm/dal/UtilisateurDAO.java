package fr.eni.projetqcm.dal;

import java.util.List;

import fr.eni.projetqcm.bo.Utilisateur;

public interface UtilisateurDAO {
	public void insert(Utilisateur utilisateur);
	public void update(Utilisateur utilisateur);
	public void delete(int id);
	public List<Utilisateur> selectAll();
	public Utilisateur selectById(int id);
	public Utilisateur selectByEmail(String email);
	public List<Utilisateur> selectByMasque(Utilisateur utilisateur, int idTest);
	public List<Utilisateur> selectByIdTest(Utilisateur utilisateur, int idTest);
}
