package fr.eni.projetqcm.dal;

import java.util.List;

import fr.eni.projetqcm.bo.Promotion;


public interface PromotionDAO {
	public void insert(Promotion promotion);
	public void update(Promotion promotion);
	public void delete(int id);
	public List<Promotion> selectAll();
	public List<Promotion> selectPromotionByMasque(String promotion);
	public Promotion selectById(int id);
}
