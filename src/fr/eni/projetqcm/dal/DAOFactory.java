package fr.eni.projetqcm.dal;

public abstract class DAOFactory {
	
	public static UtilisateurDAO getUtilisateurDAO()
	{
		return new UtilisateurDAOJdbcImpl();
	}
	
	public static TestDAO getTestDAO()
	{
		return new TestDAOJdbclmpl();
	}
	
	public static EpreuveDAO getEpreuveDAO()
	{
		return new EpreuveDAOJdbcImpl();
	}
	
	public static QuestionDAO getQuestionDAO()
	{
		return new QuestionDAOJdbclmpl();
	}
	
	public static QuestionTirageDAO getQuestionTirageDAO() {
		return new QuestionTirageDAOJdbclmpl();
	}
	
	public static ReponseTirageDAO getReponseTirageDAO() {
		return new ReponseTirageDAOJdbclmpl();
	}
	
	public static PromotionDAO getPromotionDAO() {
		return new PromotionDAOJdbcImpl();
	}
}
