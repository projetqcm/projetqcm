package fr.eni.projetqcm.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Proposition;
import fr.eni.projetqcm.bo.Question;
import fr.eni.projetqcm.bo.QuestionTirage;
import fr.eni.projetqcm.bo.SectionTest;
import fr.eni.projetqcm.bo.Test;
import fr.eni.projetqcm.bo.Theme;

public class QuestionTirageDAOJdbclmpl implements QuestionTirageDAO {

	private static final String SELECT_ALL = "SELECT idTest, libelle, description, duree, seuil_haut, seuil_bas"
			+ "FROM Test";
	private static final String SELECT_BY_EPREUVE_ID = "SELECT qt.estMarquee, qt.numOrdre,"
			+ " q.idQuestion, q.enonce as enonceQuestion, q.media, q.points, q.idTheme," 
			+ " p.enonce as enonceProposition, p.estBonne, p.idProposition as idProposition," 
			+ " e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule, e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur," 
			+ " t.idTest as idTest, t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas," 
			+ " st.nbQuestionsATirer," 
			+ " tm.libelle as libelleTheme, tm.idTheme as idTheme" 
			+ " FROM QUESTION_TIRAGE as qt" 
			+ " INNER JOIN QUESTION as q ON qt.idQuestion = q.idQuestion" 
			+ " INNER JOIN PROPOSITION as p ON q.idQuestion = p.idQuestion" 
			+ " INNER JOIN EPREUVE as e ON qt.idEpreuve = e.idEpreuve" 
			+ " INNER JOIN TEST as t ON e.idTest = t.idTest" 
			+ " INNER JOIN SECTION_TEST as st ON t.idTest = st.idTest" 
			+ " INNER JOIN THEME as tm ON st.idTheme = tm.idTheme" 
			+ " WHERE qt.idEpreuve = ?"
			+ " ORDER BY qt.numOrdre ASC";
	private static final String SELECT_BY_QUESTION_TIRAGE = "SELECT qt.estMarquee, qt.numOrdre,"
			+ " q.idQuestion, q.enonce as enonceQuestion, q.media, q.points, q.idTheme," 
			+ " p.enonce as enonceProposition, p.estBonne, p.idProposition as idProposition," 
			+ " e.idEpreuve, e.dateDebutValidite, e.dateFinValidite, e.tempsEcoule, e.etat, e.note_obtenue, e.niveau_obtenu, e.idUtilisateur," 
			+ " t.idTest as idTest, t.libelle as libelleTest, t.description, t.duree, t.seuil_haut, t.seuil_bas," 
			+ " st.nbQuestionsATirer," 
			+ " tm.libelle as libelleTheme, tm.idTheme as idTheme" 
			+ " FROM QUESTION_TIRAGE as qt" 
			+ " INNER JOIN QUESTION as q ON qt.idQuestion = q.idQuestion" 
			+ " INNER JOIN PROPOSITION as p ON q.idQuestion = p.idQuestion" 
			+ " INNER JOIN EPREUVE as e ON qt.idEpreuve = e.idEpreuve" 
			+ " INNER JOIN TEST as t ON e.idTest = t.idTest" 
			+ " INNER JOIN SECTION_TEST as st ON t.idTest = st.idTest" 
			+ " INNER JOIN THEME as tm ON st.idTheme = tm.idTheme" 
			+ " WHERE qt.idEpreuve = ? AND qt.idQuestion = ?";
	private static final String INSERT_QUESTION_TIRAGE = "INSERT INTO QUESTION_TIRAGE (estMarquee, idQuestion, numordre, idEpreuve) "
			+ "VALUES (?, ?, ?, ?);";
	private static final String DELETE_QUESTION_TIRAGE = "DELETE FROM QUESTION_TIRAGE WHERE WHERE idEpreuve = ? AND idQuestion = ?";
	private static final String DELETE_QUESTION_TIRAGE_BY_EPREUVE = "DELETE FROM QUESTION_TIRAGE WHERE idEpreuve = ?";
	private static final String UPDATE_QUESTION_TIRAGE = "UPDATE QUESTION_TIRAGE SET estMarquee = ? WHERE idQuestion = ? AND numOrdre = ? AND idEpreuve = ?";

	@Override
	public void insert(QuestionTirage questionTirage) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_QUESTION_TIRAGE);
			pstmt.setBoolean(1, questionTirage.getEstMarquee());
			pstmt.setInt(2, questionTirage.getQuestion().getIdQuestion());
			pstmt.setInt(3, questionTirage.getnumOrdre());
			pstmt.setInt(4, questionTirage.getEpreuve().getIdEpreuve());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

	}

	@Override
	public void delete(int id) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_QUESTION_TIRAGE);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
	}
	
	public void deleteByEpreuve(Epreuve epr) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_QUESTION_TIRAGE_BY_EPREUVE);
			pstmt.setInt(1, epr.getIdEpreuve());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
	}
	
	@Override
	public void update(QuestionTirage qt) {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_QUESTION_TIRAGE);
			pstmt.setBoolean(1, qt.getEstMarquee());
			pstmt.setInt(2, qt.getQuestion().getIdQuestion());
			pstmt.setInt(3, qt.getnumOrdre());
			pstmt.setInt(4, qt.getEpreuve().getIdEpreuve());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}
	}

	@Override
	public QuestionTirage selectByQuestion(QuestionTirage questionTirage) {
		List<Integer> listIdProposition = null;
		List<Integer> listIdTheme = null;
		QuestionTirage qt = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_QUESTION_TIRAGE);
			pstmt.setInt(1, questionTirage.getEpreuve().getIdEpreuve());
			pstmt.setInt(2, questionTirage.getQuestion().getIdQuestion());
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne) {
					qt = new QuestionTirage(rs.getBoolean("estMarquee"),
							new Question(rs.getInt("idQuestion"), rs.getString("enonceQuestion"), rs.getString("media"),
									rs.getInt("points"), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme")),
									new ArrayList<Proposition>()),
							rs.getInt("numOrdre"),
							new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
									rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
									rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
									new Test(rs.getInt("idTest"), rs.getString("libelleTest"),
											rs.getString("description"), rs.getInt("duree"), rs.getInt("seuil_haut"),
											rs.getInt("seuil_bas"), new ArrayList<SectionTest>()),
									rs.getInt("idUtilisateur")));
					listIdProposition = new ArrayList<Integer>();
					listIdTheme = new ArrayList<Integer>();
					premiereligne = false;
				}
				if(!listIdProposition.contains(rs.getInt("idProposition"))) {
					listIdProposition.add(rs.getInt("idProposition"));
					qt.getQuestion().addProposition(new Proposition(rs.getInt("idProposition"), rs.getString("enonceProposition"), rs.getBoolean("estBonne"), qt.getQuestion()));
				}
				if(!listIdTheme.contains(rs.getInt("idTheme"))) {
					listIdTheme.add(rs.getInt("idTheme"));
					qt.getEpreuve().getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"), qt.getEpreuve().getTest(),
						new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
				}
				
				

			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return qt;
	}
	
	@Override
	public List<QuestionTirage> selectAll() {

		List<QuestionTirage> ListQuestionTirage = new ArrayList<QuestionTirage>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
//				ListQuestionTirage.add(new Test(rs.getInt("idTest"), rs.getString("libelle"), rs.getString("description"),
//						rs.getInt("duree"), rs.getInt("seuil_haut"), rs.getInt("seuil_bas")));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return ListQuestionTirage;
	}

	@Override
	public List<QuestionTirage> selectByEpreuveId(int id) {
		List<Integer> listIdProposition = null;
		List<Integer> listIdTheme = null;
		List<QuestionTirage> list = new ArrayList<QuestionTirage>();
		QuestionTirage qt = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_EPREUVE_ID);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			Boolean premiereligne = true;
			while (rs.next()) {
				if (premiereligne || qt.getQuestion().getIdQuestion() != rs.getInt("idQuestion")) {
					qt = new QuestionTirage(rs.getBoolean("estMarquee"),
							new Question(rs.getInt("idQuestion"), rs.getString("enonceQuestion"), rs.getString("media"),
									rs.getInt("points"), new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme")),
									new ArrayList<Proposition>()),
							rs.getInt("numOrdre"),
							new Epreuve(rs.getInt("idEpreuve"), rs.getDate("dateDebutValidite"),
									rs.getDate("dateFinValidite"), rs.getInt("tempsEcoule"), rs.getString("etat"),
									rs.getFloat("note_obtenue"), rs.getString("niveau_obtenu"),
									new Test(rs.getInt("idTest"), rs.getString("libelleTest"),
											rs.getString("description"), rs.getInt("duree"), rs.getInt("seuil_haut"),
											rs.getInt("seuil_bas"), new ArrayList<SectionTest>()),
									rs.getInt("idUtilisateur")));
					list.add(qt);
					listIdProposition = new ArrayList<Integer>();
					listIdTheme = new ArrayList<Integer>();
					premiereligne = false;
				}
				if(!listIdProposition.contains(rs.getInt("idProposition"))) {
					listIdProposition.add(rs.getInt("idProposition"));
					qt.getQuestion().addProposition(new Proposition(rs.getInt("idProposition"), rs.getString("enonceProposition"), rs.getBoolean("estBonne"), qt.getQuestion()));
				}
				if(!listIdTheme.contains(rs.getInt("idTheme"))) {
					listIdTheme.add(rs.getInt("idTheme"));
					qt.getEpreuve().getTest().addSection(new SectionTest(rs.getInt("nbQuestionsATirer"), qt.getEpreuve().getTest(),
						new Theme(rs.getInt("idTheme"), rs.getString("libelleTheme"))));
				}
				
				

			}
		} catch (Exception e) {
			e.printStackTrace();
//			BusinessException businessException = new BusinessException(CodesResultatDAL.LECTURE_LISTES_ECHEC);
//			throw businessException;
		}

		return list;
	}

}
