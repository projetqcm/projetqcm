package fr.eni.projetqcm.filters;

import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class FiltreLogin
 */
@WebFilter(dispatcherTypes = {
		DispatcherType.REQUEST 
		}
		, urlPatterns = { "/*" })
public class FiltreLogin implements Filter {

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}
	
	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session = httpRequest.getSession();

		// Le cookie d'acceptation des CGU est il transmis ?
		boolean logged = false;
		if (session.getAttribute("utilisateurSession") != null) {
			logged = true;
		}
		// L'utilisateur vient d'accepter les CGU
		boolean loginEnCours = false;
		if (httpRequest.getServletPath().startsWith("/connexion")) {
			loginEnCours = true;
		}
				
		// Laisser passer la requete dans plusieurs cas:
		// 1- l'utilisateur vient d'accepter les CGU (la requete est issue de l'action d'acceptation)
		// 2- l'utilisateur a déjà acepté les CGU (lors d'une utilisation préédente de l'application)
		// 3- les ressources demandées ne necessitent pas l'acceptation des CGU (images, css, etc.)
		if( loginEnCours || 
			logged ||
			httpRequest.getServletPath().startsWith("/media") ||
			httpRequest.getServletPath().startsWith("/vendor")
			)
		{			
			chain.doFilter(request, response);
		}
		// Rediriger vers la page des CGU en "se souvenant" de la page initialement demandée
		else
		{
			httpRequest.setAttribute("urlCible", httpRequest.getContextPath()+httpRequest.getServletPath());
			RequestDispatcher rd = httpRequest.getRequestDispatcher("/WEB-INF/connexion.jsp");
			rd.forward(httpRequest, httpResponse);
		}
	}



}
