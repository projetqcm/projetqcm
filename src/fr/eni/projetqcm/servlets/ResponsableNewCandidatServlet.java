package fr.eni.projetqcm.servlets;

import java.awt.List;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.projetqcm.bll.PromotionManager;
import fr.eni.projetqcm.bll.UtilisateurManager;
import fr.eni.projetqcm.bo.Promotion;

/**
 * Servlet implementation class ResponsableNewCandidat
 */
@WebServlet("/responsable/newcandidat")
public class ResponsableNewCandidatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResponsableNewCandidatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Promotion> listPromotion = new ArrayList<Promotion>();
		PromotionManager promotionManager = new PromotionManager();
		listPromotion =  (ArrayList<Promotion>) promotionManager.selectAll();
		request.setAttribute("listPromotion", listPromotion);
		request.getRequestDispatcher("/WEB-INF/responsableNewCandidat.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response){
		
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		
		utilisateurManager.insertUtilisateur(request);
		
		try {
			response.getWriter().print("succes");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
