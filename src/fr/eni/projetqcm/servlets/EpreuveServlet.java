package fr.eni.projetqcm.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.projetqcm.bll.EpreuveManager;
import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Utilisateur;

/**
 * Servlet implementation class EpreuveServlet
 */
@WebServlet(name = "Epreuve", urlPatterns = { "/epreuve" })
public class EpreuveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EpreuveManager epreuveManager = new EpreuveManager();
		List<Epreuve> list = epreuveManager.getEpreuvesByUserIdAp((Utilisateur) request.getSession().getAttribute("utilisateurSession"));
		request.setAttribute("epreuves", list);
		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/epreuve.jsp");
		rd.forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
