package fr.eni.projetqcm.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import fr.eni.projetqcm.bll.TestManager;
import fr.eni.projetqcm.bll.UtilisateurManager;
import fr.eni.projetqcm.bo.Test;
import fr.eni.projetqcm.bo.Utilisateur;

/**
 * Servlet implementation class ResponsableInscription
 */
@WebServlet("/responsable/inscriptions")
public class ResponsableInscriptionServelt extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResponsableInscriptionServelt() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Test> listTest = new ArrayList<Test>();
		TestManager testManager = new TestManager();
		listTest = testManager.selectAll();
		request.setAttribute("listTest", listTest);
		request.getRequestDispatcher("/WEB-INF/responsableInscription.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}
