package fr.eni.projetqcm.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.projetqcm.bll.EpreuveManager;
import fr.eni.projetqcm.bll.TirageManager;
import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Utilisateur;

/**
 * Servlet implementation class Resultat
 */
@WebServlet("/qcm/resultat")
public class ResultatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResultatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EpreuveManager eprManager= new EpreuveManager();
		Integer eprId = Integer.parseInt(request.getParameter("idEpreuve"));
		request.setAttribute("epreuve", eprManager.getEpreuveById(eprId));
		request.getRequestDispatcher("/WEB-INF/resultatQcm.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
