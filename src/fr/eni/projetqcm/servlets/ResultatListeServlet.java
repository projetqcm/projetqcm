package fr.eni.projetqcm.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.projetqcm.bll.EpreuveManager;
import fr.eni.projetqcm.bo.Epreuve;
import fr.eni.projetqcm.bo.Utilisateur;

/**
 * Servlet implementation class ResultatListeServlet
 */
@WebServlet(name = "Resultat", urlPatterns = { "/resultat" })
public class ResultatListeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResultatListeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EpreuveManager epreuveManager = new EpreuveManager();
		List<Epreuve> list = epreuveManager.getEpreuvesByUserIdT((Utilisateur) request.getSession().getAttribute("utilisateurSession"));
		request.setAttribute("epreuves", list);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/resultat.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
