package fr.eni.projetqcm.servlets;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.projetqcm.bll.TirageManager;

/**
 * Servlet implementation class QcmServlet
 */
@WebServlet(name = "Qcm", urlPatterns = { "/qcm" })
public class QcmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TirageManager qT = new TirageManager();
		Integer eprId = Integer.parseInt(request.getParameter("idEpreuve"));
		request.setAttribute("questionsTirage", qT.getQuestionTirage(eprId));
		request.setAttribute("reponsesTirage", qT.getReponseTirage(eprId));
		request.getRequestDispatcher("/WEB-INF/qcm.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
	}

}
