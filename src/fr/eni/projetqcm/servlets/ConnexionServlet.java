package fr.eni.projetqcm.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.projetqcm.bll.UtilisateurManager;
import fr.eni.projetqcm.bo.Utilisateur;

/**
 * Servlet implementation class connexion
 */
@WebServlet("/connexion")
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		Utilisateur utilisateur;
		utilisateur = utilisateurManager.loginUtilisateur(request);
		
		HttpSession session = request.getSession();
		RequestDispatcher rd = null;

		if(!utilisateurManager.getErreurs()){
			session.setAttribute("utilisateurSession", utilisateur);
			response.sendRedirect(request.getContextPath() + "/accueil");			
		}else{
			request.setAttribute("utilisateur", utilisateur);
			request.setAttribute("utilisateurManager", utilisateurManager);
			session.setAttribute("utilisateurSession", null);
			request.getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
		}
	}

}
