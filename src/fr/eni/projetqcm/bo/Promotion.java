package fr.eni.projetqcm.bo;

public class Promotion {
	
	private String codePromo;
	private String libelle;
	
	public Promotion(String codePromo, String libelle) {
		super();
		this.codePromo = codePromo;
		this.libelle = libelle;
	}
	
	public Promotion() {
		
	}

	public String getCodePromo() {
		return codePromo;
	}

	public void setCodePromo(String codePromo) {
		this.codePromo = codePromo;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "promotion [codePromo=" + codePromo + ", libelle=" + libelle + "]";
	}
}
