package fr.eni.projetqcm.bo;

public class Profil {
	private Integer codeProfil;
	private String libelle;
	
	public Profil(Integer codeProfil, String libelle) {
		super();
		this.codeProfil = codeProfil;
		this.libelle = libelle;
	}
	
	public Profil() {
		
	}

	public Integer getCodeProfil() {
		return codeProfil;
	}

	public void setCodeProfil(Integer codeProfil) {
		this.codeProfil = codeProfil;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "profil [codeProfil=" + codeProfil + ", libelle=" + libelle + "]";
	}
	
}
