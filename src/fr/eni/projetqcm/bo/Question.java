package fr.eni.projetqcm.bo;

import java.util.ArrayList;
import java.util.List;

public class Question {
	
	private Integer idQuestion;
	private String enonce;
	private String media;
	private Integer points;
	private Theme theme;
	private List<Proposition> propositions;
	
	public Question() {
		this.theme = new Theme();
		this.propositions = new ArrayList<Proposition>();
	}

	public Question(Integer idQuestion, String enonce, String media, Integer points, Theme theme, List<Proposition> propositions) {
		super();
		this.idQuestion = idQuestion;
		this.enonce = enonce;
		this.media = media;
		this.points = points;
		this.theme = theme;
		this.propositions = propositions;
	}

	public Integer getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(Integer idQuestion) {
		this.idQuestion = idQuestion;
	}

	public String getEnonce() {
		return enonce;
	}

	public void setEnonce(String enonce) {
		this.enonce = enonce;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	
	public List<Proposition> getPropositions(){
		return propositions;
	}
	
	public void setPropositions(List<Proposition> propositions) {
		this.propositions = propositions;
	}
	
	public void addProposition(Proposition proposition) {
		this.propositions.add(proposition);
	}
	
	@Override
	public boolean equals(Object obj) {
		return (this.idQuestion == ((Question) obj).idQuestion);
	}

	@Override
	public String toString() {
		return "Question [idQuestion=" + idQuestion + ", enonce=" + enonce + ", media=" + media + ", points=" + points
				+ ", Theme=" + theme.toString() + ", propositions=" + propositions.toString() + "]";
	}
	
}
