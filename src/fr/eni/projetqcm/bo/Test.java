package fr.eni.projetqcm.bo;

import java.util.List;

public class Test {
	
	private Integer idTest;
	private String libelle;
	private String description;
	private Integer duree;
	private Integer seuil_haut;
	private Integer seuil_bas;
	private List<SectionTest> sections;
	
	public Test() {
		
	}
	
	public Test(Integer idTest, String libelle, String description, Integer duree, Integer seuil_haut,
			Integer seuil_bas, List<SectionTest> sections) {
		super();
		this.idTest = idTest;
		this.libelle = libelle;
		this.description = description;
		this.duree = duree;
		this.seuil_haut = seuil_haut;
		this.seuil_bas = seuil_bas;
		this.sections = sections;
	}

	public Integer getIdTest() {
		return idTest;
	}

	public void setIdTest(Integer idTest) {
		this.idTest = idTest;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDuree() {
		return duree;
	}

	public void setDuree(Integer duree) {
		this.duree = duree;
	}

	public Integer getSeuil_haut() {
		return seuil_haut;
	}

	public void setSeuil_haut(Integer seuil_haut) {
		this.seuil_haut = seuil_haut;
	}

	public Integer getSeuil_bas() {
		return seuil_bas;
	}

	public void setSeuil_bas(Integer seuil_bas) {
		this.seuil_bas = seuil_bas;
	}
	
	public List<SectionTest> getSections() {
		return sections;
	}
	
	public void setSections(List<SectionTest> sections) {
		this.sections = sections;
	}
	
	public void addSection(SectionTest section) {
		this.sections.add(section);
	}

	@Override
	public boolean equals(Object obj) {
		return (this.idTest.equals(((Test)obj).idTest)
				&& this.libelle.equals(((Test)obj).libelle)
				&& this.description.equals(((Test)obj).description)
				&& this.duree.equals(((Test)obj).duree)
				&& this.seuil_haut.equals(((Test)obj).seuil_haut)
				&& this.seuil_bas.equals(((Test)obj).seuil_bas)
				&& this.sections.equals(((Test)obj).sections));
	}

	@Override
	public String toString() {
		return "test [idTest=" + idTest + ", libelle=" + libelle + ", description=" + description + ", duree=" + duree
				+ ", seuil_haut=" + seuil_haut + ", seuil_bas=" + seuil_bas + ", sections=" + sections + "]";
	}
	
}	
