package fr.eni.projetqcm.bo;

public class ReponseTirage {

	private Proposition proposition;
	private QuestionTirage question;
	private Epreuve epreuve;

	public ReponseTirage(Proposition proposition, QuestionTirage question, Epreuve epreuve) {
		super();
		this.proposition = proposition;
		this.question = question;
		this.epreuve = epreuve;
	}
	
	public ReponseTirage() {
		
	}

	public Proposition getProposition() {
		return proposition;
	}

	public void setProposition(Proposition proposition) {
		this.proposition = proposition;
	}

	public QuestionTirage getQuestionTirage() {
		return question;
	}

	public void setQuestionTirage(QuestionTirage question) {
		this.question = question;
	}

	public Epreuve getEpreuve() {
		return epreuve;
	}

	public void setEpreuve(Epreuve epreuve) {
		this.epreuve = epreuve;
	}

	@Override
	public String toString() {
		return "ReponseTirage [proposition=" + proposition + ", question=" + question + ", epreuve=" + epreuve + "]";
	}

}
