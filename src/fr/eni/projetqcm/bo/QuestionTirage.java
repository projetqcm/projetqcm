package fr.eni.projetqcm.bo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="questionTirage")
public class QuestionTirage {

	private Boolean estMarquee;
	private Question question;
	private Integer numOrdre;
	private Epreuve epreuve;

	public QuestionTirage() {
		this.question = new Question();
		this.epreuve = new Epreuve();
	}

	public QuestionTirage(Boolean estMarquee, Question question, Integer numOrdre, Epreuve epreuve) {
		super();
		this.estMarquee = estMarquee;
		this.question = question;
		this.numOrdre = numOrdre;
		this.epreuve = epreuve;
	}

	public Boolean getEstMarquee() {
		return estMarquee;
	}

	public void setEstMarquee(Boolean estMarquee) {
		this.estMarquee = estMarquee;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Integer getnumOrdre() {
		return numOrdre;
	}

	public void setnumOrdre(Integer numOrdre) {
		this.numOrdre = numOrdre;
	}

	public Epreuve getEpreuve() {
		return epreuve;
	}

	public void setEpreuve(Epreuve epreuve) {
		this.epreuve = epreuve;
	}

	@Override
	public boolean equals(Object obj) {
		return (this.estMarquee.equals(((QuestionTirage) obj).estMarquee)
				&& this.numOrdre.equals(((QuestionTirage) obj).numOrdre)
				&& this.question.equals(((QuestionTirage) obj).question)
				&& this.epreuve.equals(((QuestionTirage) obj).epreuve));
	}

	@Override
	public String toString() {
		return "QuestionTirage [estMarquee=" + estMarquee + ", idQuestion=" + question + ", numOrdre=" + numOrdre
				+ ", idEpreuve=" + epreuve + "]";
	}

}
