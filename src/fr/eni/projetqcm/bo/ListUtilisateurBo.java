package fr.eni.projetqcm.bo;

import java.util.List;

public class ListUtilisateurBo {
	private List<Utilisateur> listUtilisateur;
	
	public ListUtilisateurBo(List<Utilisateur> listUtilisateur) {
		this.listUtilisateur = listUtilisateur;
	}

	public List<Utilisateur> getListUtilisateur() {
		return listUtilisateur;
	}

	public void setListUtilisateur(List<Utilisateur> listUtilisateur) {
		this.listUtilisateur = listUtilisateur;
	}
	
	
}
