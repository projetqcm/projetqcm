package fr.eni.projetqcm.bo;

public class SectionTest {
	
	private Integer nbQuestionsATirer;
	private Test test;
	private Theme theme;
	
	public SectionTest(Integer nbQuestionsATirer, Test test, Theme theme) {
		super();
		this.nbQuestionsATirer = nbQuestionsATirer;
		this.test = test;
		this.theme = theme;
	}

	public Integer getNbQuestionsATirer() {
		return nbQuestionsATirer;
	}

	public void setNbQuestionsATirer(Integer nbQuestionsATirer) {
		this.nbQuestionsATirer = nbQuestionsATirer;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (this.nbQuestionsATirer.equals(((SectionTest)obj).nbQuestionsATirer)
				&& this.theme.equals(((SectionTest)obj).theme));
	}

	@Override
	public String toString() {
		return "SectionTest [nbQuestionsATirer=" + nbQuestionsATirer + ", theme=" + theme.toString()
				+ "]";
	}
	
}
