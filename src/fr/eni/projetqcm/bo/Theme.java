package fr.eni.projetqcm.bo;

import java.util.ArrayList;
import java.util.List;

public class Theme {
	
	private Integer idTheme;
	private String libelle;
	private List<Question> questions;
	
	public Theme() {
		this.questions = new ArrayList<Question>();
	}
	
	public Theme(Integer idTheme, String libelle) {
		super();
		this.idTheme = idTheme;
		this.libelle = libelle;
	}

	public Integer getIdTheme() {
		return idTheme;
	}

	public void setIdTheme(Integer idTheme) {
		this.idTheme = idTheme;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public List<Question> getQuestions(){
		return questions;
	}
	
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	public void addQuestion(Question question) {
		this.questions.add(question);
	}
	
	@Override
	public boolean equals(Object obj) {
		return (this.idTheme.equals(((Theme)obj).idTheme)
				&& this.libelle.equals(((Theme)obj).libelle));
	}

	@Override
	public String toString() {
		return "Theme [idTheme=" + idTheme + ", libelle=" + libelle + "]";
	}
	
}
