package fr.eni.projetqcm.bo;

import java.util.Date;

public class Epreuve {
	
	private Integer idEpreuve;
	private Date dateDebutValidite;
	private Date dateFinValidite;
	private Integer tempsEcoule;
	private String etat;
	private Float note_obtenue;
	private String niveau_obtenu;
	private Test test;
	private Integer idUtilisateur;
	private Utilisateur utilisateur;
	
	public Epreuve() {
		this.test = new Test();
	}
	
	public Epreuve(Integer idEpreuve, Date dateDebutValidite, Date dateFinValidite, Integer tempsEcoule, String etat,
			Float note_obtenue, String niveau_obtenu, Test test, Integer idUtilisateur) {
		super();
		this.idEpreuve = idEpreuve;
		this.dateDebutValidite = dateDebutValidite;
		this.dateFinValidite = dateFinValidite;
		this.tempsEcoule = tempsEcoule;
		this.etat = etat;
		this.note_obtenue = note_obtenue;
		this.niveau_obtenu = niveau_obtenu;
		this.test = test;
		this.idUtilisateur = idUtilisateur;
	}
	
	public Epreuve(Date dateDebutValidite, Date dateFinValidite, Integer tempsEcoule, String etat,
			Float note_obtenue, String niveau_obtenu, Test test, Integer idUtilisateur) {
		super();
		this.dateDebutValidite = dateDebutValidite;
		this.dateFinValidite = dateFinValidite;
		this.tempsEcoule = tempsEcoule;
		this.etat = etat;
		this.note_obtenue = note_obtenue;
		this.niveau_obtenu = niveau_obtenu;
		this.test = test;
		this.idUtilisateur = idUtilisateur;
	}
	
	public Epreuve(Integer idEpreuve, Date dateDebutValidite, Date dateFinValidite, Integer tempsEcoule, String etat,
			Float note_obtenue, String niveau_obtenu, Utilisateur utilisateur) {
		super();
		this.idEpreuve = idEpreuve;
		this.dateDebutValidite = dateDebutValidite;
		this.dateFinValidite = dateFinValidite;
		this.tempsEcoule = tempsEcoule;
		this.etat = etat;
		this.note_obtenue = note_obtenue;
		this.niveau_obtenu = niveau_obtenu;
		this.utilisateur = utilisateur;
	}
	
	public Integer getIdEpreuve() {
		return idEpreuve;
	}
	public void setIdEpreuve(Integer idEpreuve) {
		this.idEpreuve = idEpreuve;
	}
	public Date getdateDebutValidite() {
		return dateDebutValidite;
	}
	public void setdateDebutValidite(Date dateDebutValidite) {
		this.dateDebutValidite = dateDebutValidite;
	}
	public Date getDateFinValidite() {
		return dateFinValidite;
	}
	public void setDateFinValidite(Date dateFinValidite) {
		this.dateFinValidite = dateFinValidite;
	}
	public Integer getTempsEcoule() {
		return tempsEcoule;
	}
	public void setTempsEcoule(Integer tempsEcoule) {
		this.tempsEcoule = tempsEcoule;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public Float getNote_obtenue() {
		return note_obtenue;
	}
	public void setNote_obtenue(Float note_obtenue) {
		this.note_obtenue = note_obtenue;
	}
	public String getNiveau_obtenu() {
		return niveau_obtenu;
	}
	public void setNiveau_obtenu(String niveau_obtenu) {
		this.niveau_obtenu = niveau_obtenu;
	}
	public Test getTest() {
		return test;
	}
	public void setTest(Test test) {
		this.test = test;
	}
	public Integer getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(Integer idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (this.idEpreuve.equals(((Epreuve) obj).idEpreuve)
				&& this.dateDebutValidite.equals(((Epreuve)obj).dateDebutValidite)
				&& this.dateFinValidite.equals(((Epreuve)obj).dateFinValidite)
				&& this.tempsEcoule.equals(((Epreuve)obj).tempsEcoule)
				&& this.etat.equals(((Epreuve)obj).etat)
				&& this.note_obtenue.equals(((Epreuve)obj).note_obtenue)
				&& this.niveau_obtenu.equals(((Epreuve)obj).niveau_obtenu)
				&& this.test.equals(((Epreuve)obj).test)
				&& this.idUtilisateur.equals(((Epreuve)obj).idUtilisateur));
	}
	
	
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	@Override
	public String toString() {
		return "Epreuve [idEpreuve=" + idEpreuve + ", dateDebutValidite=" + dateDebutValidite + ", dateFinValidite="
				+ dateFinValidite + ", tempsEcoule=" + tempsEcoule + ", etat=" + etat + ", note_obtenue=" + note_obtenue
				+ ", niveau_obtenu=" + niveau_obtenu + ", Test=" + test + ", idUtilisateur=" + idUtilisateur + "]";
	}
	
	
}


