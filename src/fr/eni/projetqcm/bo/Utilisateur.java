package fr.eni.projetqcm.bo;

public class Utilisateur {

	private Integer id;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	private Profil profil;
	private Promotion promotion;
	private String salt;
	private int idTest;

	public Utilisateur() {

	}

	public Utilisateur(Integer id, String nom, String prenom, String email, String password, Profil profil, Promotion promotion, String salt) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		this.profil = profil;
		this.promotion = promotion;
		this.salt = salt;
	}
	
	public Utilisateur(String nom, String prenom, String email, String password, Profil profil, Promotion promotion) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		this.profil = profil;
		this.promotion = promotion;
	}
	
	public Utilisateur(Integer id, String nom, String prenom, String email) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
	}
	
	public Utilisateur(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public Utilisateur(int idTest) {
		super();
		this.idTest= idTest;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setCodePromo(Promotion promotion) {
		this.promotion = promotion;
	}
	
	public String getSalt() {
		return salt;
	}
	
	public void setSalt(String salt) {
		this.salt = salt;
	}

	
	public int getIdTest() {
		return idTest;
	}

	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}

	@Override
	public String toString() {
		return "utilisateur [idUtilisateur=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email="
				+ email + ", password=" + password + ", profil=" + profil + ", promotion=" + promotion + "]";
	}

}
