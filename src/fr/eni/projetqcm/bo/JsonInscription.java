package fr.eni.projetqcm.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonInscription implements Serializable{

	private static final long serialVersionUID = 4048841131123223720L;
	private List<Integer> idListUser;
	private int idTest;
	private Date dateDebutValidite;
	private Date dateFinValidite;
	private Boolean isValid = true;
	
	public JsonInscription() {
		super();
	}

	public JsonInscription(List<Integer> idListUser, int idTest, Date dateDebutValidite, Date dateFinValidite) {
		super();
		this.idListUser = idListUser;
		this.idTest = idTest;
		this.dateDebutValidite = dateDebutValidite;
		this.dateFinValidite = dateFinValidite;
	}
	
	public List<Integer> getIdListUser() {
		return idListUser;
	}

	public void setIdListUser(List<Integer> idListUser) {
		this.idListUser = idListUser;
	}

	public Integer getIdTest() {
		return idTest;
	}
	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}
	public Date getDateDebutValidite() {
		return dateDebutValidite;
	}
	public void setDateDebutValidite(Date dateDebutValidite) {
		
		this.dateDebutValidite = dateDebutValidite;
	}
	public Date getDateFinValidite() {
		return dateFinValidite;
	}
	public void setDateFinValidite(Date dateFinValidite) {
		this.dateFinValidite = dateFinValidite;
	}
	
	public boolean isValid() {
		if(this.idListUser.isEmpty()) {
			this.isValid = false;
		}else if(this.idTest == 0) {
			this.isValid = false;
		}else if(this.getDateDebutValidite() == null) {
			this.isValid = false;
		}else if(this.getDateFinValidite() == null) {
			this.isValid = false;
		}
		
		return isValid;
	}


	@Override
	public String toString() {
		return "JsonInscription [idListUser=" + idListUser + ", idTest=" + idTest + ", dateDebutValidite="
				+ dateDebutValidite + ", dateFinValidite=" + dateFinValidite + "]";
	}
	
}
