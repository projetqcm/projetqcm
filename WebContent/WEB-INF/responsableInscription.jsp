<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="./layout/header.jspf" %>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha256-Md8eaeo67OiouuXAi8t/Xpd8t2+IaJezATVTWbZqSOw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
	<link href="${pageContext.request.contextPath}/media/css/inscriptionTest.css" rel="stylesheet"/>
    <title>Inscription Test</title>
</head>
	<body>
	<%@ include file="./layout/navbar.jspf" %>
		<div class="container-fluid">
			<div class="desc_epreuve">
				<p>Inscription aux tests</p>
			</div>
			<div id="colum_inscription_body">
				<div class="left_inscription">
					<form id="form_inscription_test_search" method="post" action="${pageContext.request.contextPath}/rs/responsable/searchcandidat">
						<label for="search_candidat">Recherche des candidats</label>
						<p><input type="text" name="search_candidat" id="search_candidat" placeholder="Rechercher"></p>
						<label for="name_search">Nom</label>
						<input type="radio" name="type_search" id="name_search" value="name_search" checked="checked">
						<label for="promotion_search">Promotion</label>
						<input type="radio" name="type_search" id="promotion_search" value="promotion_search">
					</form>
					<div id="list_result_search">
						<p>Candidats trouvés</p>
						<form id="transfert_result_search" method="post" action="${pageContext.request.contextPath}/rs/responsable/inscriptionepreuve">
							
						</form>
						<button id="submit_transfert_result_search" class="btn btn-info">Inscrire</button>
					</div>	
				</div>
				<div class="right_inscription">
					<div id="block_selection_test">
						<form id="form_selection-test" method="post" action="#">
							<label for="test_search">Sélectionner le test</label>
							<select class="form-control" id="test_search" name="test_search">
							  	<c:forEach items="${ listTest }" var="test">
							    	<option id="test_${ test.idTest }">${ test.libelle }</option>
							    </c:forEach>
							</select>
							<p for="date_search_1">Période de validité d'inscription</p>
				            <div class="form-group-date">
				            	<strong> du </strong>
				                <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
				                    <input id="date_search_1" name="date_search_1" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
				                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
				                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
				                    </div>
				                </div>
				                <strong> au </strong>
				                <div class="input-group date" id="datetimepicker5" data-target-input="nearest">
				                    <input id="date_search_2" name="date_search_2" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
				                    <div class="input-group-append" data-target="#datetimepicker5" data-toggle="datetimepicker">
				                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
				                    </div>
				                </div>
				            </div>
							<div class="alert alert-danger alert_date" role="alert">
							  Veuillez renseigner les Dates
							</div>
						</form>
					</div>
					<div id="result_selection_test">
						<p>Candidats inscrits</p>
						<form id="list_candidat_inscrit" action="${pageContext.request.contextPath}/rs/responsable/candidatinscrit" method="post">
				
						</form>
						<button id="submit_list_candidat_inscrit" class="btn btn-info">Désinscrire</button>
					</div>
				</div>
			</div>

		</div>
		<footer>
			<%@ include file="./layout/footer.jspf" %>
			<script src="${pageContext.request.contextPath}/media/js/moment.js"></script>	
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
			<script src="${pageContext.request.contextPath}/media/js/inscriptionTest.js"></script>
				
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#datetimepicker5').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
		</footer>
	</body>
</html>