<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file="./layout/header.jspf" %>
	    <title>Nouveau candidat</title>
	</head>
	<body>
	<%@ include file="./layout/navbar.jspf" %>
	<div class="container-fluid">
		<div class="desc_epreuve">
			<p>Nouveau Candidat</p>
		</div>
		<div id="form_new_candidat">
			<form mathod="post" action="${pageContext.request.contextPath}/responsable/newcandidat?candidat=1">
				<label for="name_candidat">Nom</label>
				<input type="text" id="name_candidat" name="name_candidat" required>
				<label for="prenom_candidat">Prenom</label>
				<input type="text" id="prenom_candidat" name="prenom_candidat" required>
				<label for="email_candidat">Email</label>
				<input type="text" id="email_candidat" name="email_candidat" required>
				<label for="promotion_candidat">Promotion</label>
				  <select class="form-control" id="promotion_candidat" name="promotion_candidat">
				  	<c:forEach items="${ listPromotion }" var="promotion">
				    	<option>${ promotion.codePromo }</option>
				    </c:forEach>
				  </select>		
				<button id="btn_newCandidat" type="submit" class="btn btn-info">Inscrire le candidat</button>			
			</form>
		</div>
	</div>
		<footer>
			<%@ include file="./layout/footer.jspf" %>
			<script src="${pageContext.request.contextPath}/media/js/newCandidat.js"></script>
		</footer>
	</body>
</html>