<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="./layout/header.jspf" %>
    <title>Accueil</title>
</head>
<body>
	<%@ include file="./layout/navbar.jspf" %>

	<footer>
	<%@ include file="./layout/footer.jspf" %>
	</footer>
</body>
</html>