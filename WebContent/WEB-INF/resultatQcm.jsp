<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="./layout/header.jspf"%>
<title>Résultat</title>
</head>
<body>
	<%@ include file="./layout/navbar.jspf"%>
	<input type="hidden" id="idEpreuve" data-id="${param.idEpreuve}">
	<div class="container-fluid">
		<div class="desc_epreuve">
			Resultat
		</div>
		<ul>
			<li><span id="note_obtenue">Chargement ...</span></li>
	      	<li><span id="niveau_obtenu">Chargement ...</span></li>
		</ul>
	</div>
	<%@ include file="./layout/modal.jspf"%>
	<footer>
		<%@ include file="./layout/footer.jspf"%>
		<script type="text/javascript">
			var path = '${pageContext.request.contextPath}';
		</script>
		<script src="${pageContext.request.contextPath}/media/js/resultat.js"></script>
	</footer>
</body>
</html>