<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="./layout/header.jspf" %>
    <title>Epreuve</title>
</head>
<body>

	<%@ include file="./layout/navbar.jspf" %>
	<div class="container-fluid">
		<div class="desc_epreuve">
			<h1>Vos Résultats</h1>
			<p>Voici le récapitulatif des résultats que vous avez obtenus aux épreuves</p>
		</div>
		<div id="accordion">
			<c:set var="id_test" value="0"/>
			<c:set var="test_libelle" value="0"/>
			<c:forEach items="${ epreuves }" var="epreuve" varStatus="status">
				  <div class="card">
				    <div class="card-header" id="heading${ epreuve.idEpreuve }">
				      <h5 class="mb-0">
				        <button id="libelle_test_accordion_${ epreuve.idEpreuve }" class="libelle_test_accordion btn btn-link collapsed" data-toggle="collapse" data-target="#collapse${ epreuve.idEpreuve }" aria-expanded="false" aria-controls="collapse${ epreuve.idEpreuve }">
				          ${epreuve.test.libelle}
				        </button>
				      </h5>
				    </div>
				    <div id="collapse${ epreuve.idEpreuve }" class="collapse" aria-labelledby="heading${ epreuve.idEpreuve }" data-parent="#accordion">
				      <div class="card-body">
				      <ul>		
				      	<li><span>Note Obtenue : ${ epreuve.note_obtenue }</span></li>
				      	<li><span>Niveau d'acquisition : ${epreuve.niveau_obtenu}</span></li>
				      </ul>
				    </div>
				  </div>
			</c:forEach>
		</div>	
	</div>
	
	
	<footer>
		<%@ include file="./layout/footer.jspf" %>
	</footer>
</body>
</html>