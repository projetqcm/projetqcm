<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="./layout/header.jspf" %>
    <title>Epreuve</title>
</head>
<body>

	<%@ include file="./layout/navbar.jspf" %>
	<div class="desc_epreuve">
		<p>Voici la liste des Tests auquels vous ètes inscrits.</p>
		<p>Selectionner un test à passer et confirmer son lancement.</p>
	</div>
	<div class="container-fluid">
		<div id="accordion">
			<c:set var="id_test" value="0"/>
			<c:set var="test_libelle" value="0"/>
			<c:forEach items="${ epreuves }" var="epreuve" varStatus="status">
				  <div class="card">
				    <div class="card-header" id="heading${ epreuve.idEpreuve }">
				      <h5 class="mb-0">
				        <button id="libelle_test_accordion_${ epreuve.idEpreuve }" class="libelle_test_accordion btn btn-link collapsed" data-toggle="collapse" data-target="#collapse${ epreuve.idEpreuve }" aria-expanded="false" aria-controls="collapse${ epreuve.idEpreuve }">
				          ${epreuve.test.libelle}
				        <c:set var="test_libelle" value="${epreuve.test.libelle}"/>	
				        </button>
				      </h5>
				    </div>
				    <div id="collapse${ epreuve.idEpreuve }" class="collapse" aria-labelledby="heading${ epreuve.idEpreuve }" data-parent="#accordion">
				      <div class="card-body">
				      <ul>
				      	<li><span>Incription valide du : ${epreuve.dateDebutValidite} au ${epreuve.dateFinValidite}</span></li>
				      	
				      	<c:set var = "totalQuestion" value = "0"/>		
				      	
				      	<c:forEach items="${ epreuve.test.sections }" var="sections" varStatus="status">				  
				      		<c:set var="totalQuestion" value="${totalQuestion + sections.nbQuestionsATirer}"/>						      		   
				      	</c:forEach>
				      	<li><span>Nombre de question : ${ totalQuestion }</span></li>
				      	<li><span>Durée du test : ${epreuve.test.duree}</span></li>
				      </ul>
				      	<c:set var="id_test" value="${ epreuve.test.idTest }"/>	
						<button type="button" id="epreuve${ epreuve.idEpreuve }" class="btn btn-info " data-toggle="modal" data-target="#Modal">Passer le test</button></a>			     
				    </div>
				  </div>
			</c:forEach>
		</div>	
	</div>

	<%@ include file="./layout/modal.jspf" %>

	<footer>
		<%@ include file="./layout/footer.jspf" %>
		<script type="text/javascript">
			$('#accordion .card .btn-info').on("click", function(){
				var idEpreuve = $(this).attr("id").match(/\d+/)[0];
				var libelleTest = $("#libelle_test_accordion_" + idEpreuve).text().trim();
				setEtat(idEpreuve, "EC");
				$('#Modal .modal-body p').text('Souhaitez-vous passer le test "'+libelleTest+'" ?');
				$('#link_modal').attr("href", "${pageContext.request.contextPath}/qcm?idEpreuve=" + idEpreuve);
			});
			
			function setEtat(idEpr, etat){
				var url = '${pageContext.request.contextPath}/rs/epreuve/etat';
				$.ajax({
				    url: url,
				    data: {idEpreuve : idEpr, 
				    	etat : etat
				    },
				    method: 'post',
				})
			}
		</script>
	</footer>
</body>
</html>