<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="./layout/header.jspf" %>
    <title>QCM</title>
</head>
<body>
	<%@ include file="./layout/navbar.jspf" %>
	<input type="hidden" id="idEpreuve" data-id="${param.idEpreuve}">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 list_question">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<c:forEach items="${ questionsTirage }" var="questionTirage">
						<c:if test="${questionTirage.numOrdre == 1}">
							<c:set var="tempsEcoule" value = "${questionTirage.epreuve.tempsEcoule}"/>
						</c:if>
						<a class="nav-link ${questionTirage.numOrdre == 1 ? 'active' : ''} question" id="enonce_${questionTirage.numOrdre}-tab" data-id="${questionTirage.question.idQuestion}" href="#enonce_${questionTirage.numOrdre}" role="tab" aria-controls="enonce_${questionTirage.numOrdre}" aria-selected="true">
							Question ${questionTirage.numOrdre}
							${questionTirage.estMarquee == true ? '<i class="fas fa-flag"></i>' : ''}
			          	</a>
					</c:forEach>
				</div>
			</div>
			
			<div class="tab-content col-md-8" id="v-pills-tabContent">
				<c:forEach items="${ questionsTirage }" var="questionTirage">
					<div class="tab-pane fade show ${questionTirage.numOrdre == 1 ? 'active' : ''}" id="enonce_${questionTirage.numOrdre}" role="tabpanel" aria-labelledby="enonce_${questionTirage.numOrdre}">	
						<label class="label_marquage"><input type="checkbox" class="marque_question" ${questionTirage.estMarquee == true ? 'checked' : ''}>Marquer la question </label>
						<div class="desc_epreuve">
							${questionTirage.question.enonce}
						</div>
						<div class="enonce">
							<c:if test="${not empty questionTirage.question.media}">
								<img src="${pageContext.request.contextPath}/media/image/ENIEcole.jpg">
							</c:if>
							<div class="enonce_questions">
							<c:forEach items="${questionTirage.question.propositions}" var="proposition">
								<span class="button-checkbox">
							        <button type="button" class="btn btn-primary proposition" data-id="${proposition.idProposition}">
							        	${proposition.enonce}
							        </button>
							        <input type="checkbox" class="hidden" 
								        <c:forEach items="${reponsesTirage}" var="reponseTirage">
								        	${reponseTirage.proposition.idProposition == proposition.idProposition ? 'checked' : ''}
								        </c:forEach>
							        />
							    </span>
								<!-- <button type="button" class="btn btn-primary proposition" >
									<input type="checkbox" class="custom-control-input">
								</button> -->
							</c:forEach>
							</div>
						</div>
					</div>
				</c:forEach>
				<div id="time_container">
					Temps restant : <p id="timer"></p>
				</div>
				<button type="button" class="btn btn-primary" id="next">Suivant</button>			
			</div>
		</div>
	</div>
	<%@ include file="./layout/modal.jspf" %>
	<footer>
		<%@ include file="./layout/footer.jspf" %>
		<script type="text/javascript">
			var path = '${pageContext.request.contextPath}';
			var tempsEcoule = ${tempsEcoule};
		</script>
		<script src="${pageContext.request.contextPath}/media/js/qcm.js"></script>
	</footer>
</body>
</html>