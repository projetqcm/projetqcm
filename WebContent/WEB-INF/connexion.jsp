<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<!doctype html>
<html lang="fr">
<head>
	<%@ include file="./layout/header.jspf" %>
	<title>Connexion</title>
</head>
<body>
	<div class="wrapper fadeInDown">
		<div id="formContent">
	    	<div class="fadeIn first">
	      		<img src="${pageContext.request.contextPath}/media/image/ENIEcole.jpg" id="icon" alt="User Icon" />
			</div>
		<form method="post" action="${pageContext.request.contextPath}/connexion">
			<input id="urlCible" type="hidden" data-url="${urlCible}">
			<input class="form_input_connexion" type="text" id="login" class="fadeIn second" name="login" placeholder="login" 
				value="${utilisateur.email}" />
			
			<input class="form_input_connexion" type="password" id="password" class="fadeIn third" name="password" placeholder="password">
			<c:if test="${utilisateurManager.erreurs}">
				<div class="alert alert-danger">Vos identifiants sont incorrects</div>
			</c:if>
			
			<input type="submit" class="fadeIn fourth" value="Log In">
	    </form>
		
	    <div id="formFooter">
	      <a class="underlineHover" href="#">Forgot Password?</a>
	    </div>
	  </div>
	</div>
	
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/vendor/popperjs/popper.min.js"></script>
    <script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.min.js"></script>
    
</body>
</html>