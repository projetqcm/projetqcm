    $("#form_inscription_test_search").submit(function(e){
        e.preventDefault();
    });
    
    $("#transfert_result_search").submit(function(e){
        e.preventDefault();
    });
    
    $("#form_inscription_test_search").on("input", function(){
    	ajax_search();
    });
    
    $('#test_search').on("change", function(){
    	ajax_search();
    	ajax_inscrit();
    })
    
    $('#submit_transfert_result_search').on("click", function(){
    	$('#form_selection-test .alert_date').hide();
    	var date1 = $('#date_search_1').val();
    	var date2 = $('#date_search_2').val();
    	
    	//if(date1 != "Invalid Date" && date2 != "Invalid Date"){
 
    		var userChecked = [];
    		var url = $('#transfert_result_search').attr('action');
    		
    		$("input[type='checkbox']:checked").each(function(){
    			userChecked.push($(this).attr('id').replace( /[^\d.]/g, '' ));
    		});
    		
    		var data = {
    			'idListUser': userChecked,
    			'idTest': $('#test_search').find(":selected").attr('id').replace( /[^\d.]/g, '' ),
    			'dateDebutValidite': date1,
    			'dateFinValidite': date2
    		};
    		
    		$.ajax({
    			url: url,
    			method: 'post',
    			dataType: "json",
    			contentType: "application/json",
    			data: JSON.stringify(data)
    			
    		}).done(function (result) {
    			ajax_search();
    			ajax_inscrit();
    		});
    		

    });
    
        	function ajax_search(){
    	
    			var url = $('#form_inscription_test_search').attr('action');
    		
		       	var data = {
		       			'inputsearch': $('#search_candidat').val(),
		    			'radiosearch': $('input[name=type_search]:checked', '#form_inscription_test_search').val(),
		    			'idtest': $('#test_search').find(":selected").attr('id').replace( /[^\d.]/g, '' )
		    		};

		    	$('#transfert_result_search').html('');
    	
                $.ajax({
                    url: url,
                    data: "data="+JSON.stringify(data),
                    method: 'post',
                }).done(function (result) {
                	if(result != null){

                		if($('input[name=type_search]:checked', '#form_inscription_test_search').val() === "name_search"){
                        	result.forEach(function(element){
                        		var html = 
                        		'<li>'+
                        			'<input class="inputCheckboxInscription" type="checkbox" id="user_'+element.id+'" name="user_'+element.id+'">'+
                        			'<label for="user_'+element.id+'">'+element.prenom+' '+element.nom+'</label>'+
                        		'</li>';
                        		$('#transfert_result_search').append(html);
                        	});		
                		}else{
                        	result.forEach(function(element){
                        		var html = 
                        		'<li>'+
                        			'<input class="inputCheckboxInscription" type="checkbox" id="promotion_'+element.codePromo+'">'+
                        			'<label for="promotion_'+element.codePromo+'">'+element.codePromo+'</label>'+
                        		'</li>';
                        		$('#transfert_result_search').append(html);
                        	});	
                		}
                	}
                }); 
    		};
    		
    		function ajax_inscrit(){
    			var url = $('#list_candidat_inscrit').attr('action');
        		
		       	var data = {
		    			'idtest': $('#test_search').find(":selected").attr('id').replace( /[^\d.]/g, '' )
		    		};

		    	$('#list_candidat_inscrit').html('');
    	
                $.ajax({
                    url: url,
                    data: "data="+JSON.stringify(data),
                    method: 'post',
                }).done(function (result) {
                	if(result != null){
                    	result.forEach(function(element){
                    		var html = 
                    		'<li>'+
                    			'<input class="inputCheckboxDesinscrire" type="checkbox" id="user_'+element.utilisateur.id+'" name="user_'+element.utilisateur.id+'">'+
                    			'<label for="user_'+element.utilisateur.id+'">'+element.utilisateur.nom+' '+element.utilisateur.prenom+'</label>'+
                    		'</li>';
                    		$('#list_candidat_inscrit').append(html);
                    	});		
                	}else{
                		var html = 
                    		'<li>'+
                    			'<p>Aucun candidat inscrit</p>'+
                    		'</li>';
                    		$('#list_candidat_inscrit').append(html);
                	}
                }); 
    		};
    
    
    
    