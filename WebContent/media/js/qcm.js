console.log(tempsEcoule);
// Set the date we're counting down to
var start = new Date();
start.setMinutes(start.getMinutes() + (60 - tempsEcoule));

var y = setInterval(function(){
	var url = path + '/rs/epreuve/timer';
	tempsEcoule = tempsEcoule + 1;
	$.ajax({
	    url: url,
	    data: {idEpreuve : $("#idEpreuve").attr('data-id'),
	    	tempsEcoule : tempsEcoule},
	    method: 'post',
	})
}, 60000);

// Update the count down every 1 second
var x = setInterval(function() {
	  // Get todays date and time
	  var now = new Date().getTime();

	  // Find the distance between now and the count down date
	  var distance = start - now;
//	  console.log(distance);

	  // Time calculations for hours, minutes and seconds
	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

	  // Display the result in the element with id="timer"
	  document.getElementById("timer").innerHTML = hours + "h "
	  + minutes + "m " + seconds + "s ";
	  
	  if(distance < 120000){
		  document.getElementById("timer").style.color = "#ff7f30";
	  }

	  // If the count down is finished, write some text 
	  if (distance < 0) {
	    clearInterval(x);
	    clearInterval(y);
	    document.getElementById("timer").style.color = "#ff3030";
	    document.getElementById("timer").innerHTML = "Terminé";
	    setTerminer();
	  }
	  
}, 1000);	

$("#next").on("click", function(){
	setReponse($("#v-pills-tab .active").attr('data-id'));
	if($(this).html() == 'Terminer'){
		var idEpreuve = $("#idEpreuve").attr('data-id');
		$('#Modal .modal-body p').text('Souhaitez-vous terminer le test ?');
		$('#link_modal').attr("href", path + "/qcm/resultat?idEpreuve=" + idEpreuve);
		$('#Modal').modal('show');
	} else {
		$('#v-pills-tab .active').next().tab('show');
		if ($('#v-pills-tab .active').next().attr('data-id') == null){
			setTerminer();
		}
	}
});

$("#link_modal").on("click", function(){
	setEtat("T");
});

$(".question").on("shown.bs.tab", function(e){
	setReponse($(e.relatedTarget).attr('data-id'));
	if($(this).next().attr('data-id') == null){
		setTerminer();
	} else {
		$("#next").html('Suivant');
	}
});

$(".marque_question").on("click", function(e){
	var target = e.target;
	var question = $("#" + $(e.target).parent().closest('div').attr('id') + "-tab");
	
	setChecked();
	if(target.checked){
		question.append('<i class="fas fa-flag"></i>');
	} else {
		question.find('i').remove();
	}
});

function setTerminer(){
	$("#next").html('Terminer');
}

function setChecked(){
	var url = path + '/rs/epreuve/question/marquee';
	$.ajax({
	    url: url,
	    data: {idEpreuve : $("#idEpreuve").attr('data-id'), 
	    	idQuestion : $("#v-pills-tab .active").attr('data-id'),
	    },
	    method: 'post',
	});
}

function setReponse(idQuestion){
	var url = path + '/rs/epreuve/reponse/ajouter';
	var idProps = [];
	$(".tab-pane[id=" + $(".question[data-id=" + idQuestion + "]").attr('aria-controls') + "]").find('.proposition.active').each(function(){
		idProps.push($(this).attr('data-id'));
	});
	idProps = JSON.stringify(idProps);
	//console.log($("#v-pills-tabContent .active").attr('id') + "-tab");
	
	$.ajax({
	    url: url,
	    data: {idEpreuve : $("#idEpreuve").attr('data-id'), 
	    	idQuestion : idQuestion,
	    	idPropositions : idProps
	    },
	    method: 'post',
	})
}

function setEtat(etat){
	var url = path + '/rs/epreuve/etat';
	
	$.ajax({
	    url: url,
	    data: {idEpreuve : $("#idEpreuve").attr('data-id'), 
	    	etat : etat
	    },
	    method: 'post',
	})
}

$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
