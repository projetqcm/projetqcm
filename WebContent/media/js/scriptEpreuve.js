$('#accordion .card .btn-info').on("click", function(){
		setEtat("EC");
		var idEpreuve = $(this).attr("id").match(/\d+/)[0];
		var libelleTest = $("#libelle_test_accordion_" + idEpreuve).text().trim();
		$('#ModalEpreuve .modal-body p').text('Souhaitez-vous passer le test '+libelleTest+' ?');
		$('#link_modal_epreuve').attr("href", "/projectQcm/qcm&idEpreuve=" + idEpreuve);
});

function setEtat(etat){
	var url = '/ProjetQCM/rs/epreuve/etat';
	
	$.ajax({
	    url: url,
	    data: {idEpreuve : $("#idEpreuve").attr('data-id'), 
	    	etat : etat
	    },
	    method: 'post',
	})
}