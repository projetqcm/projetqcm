    $("form").submit(function(e){
        e.preventDefault();
    });
    
    $("#form_new_candidat form").on("submit", function(){
    	var url = $(this).attr('action');
    	var name_candidat = $(this).children("#name_candidat").val();
        $.ajax({
            url: url,
            data: $(this).serialize(),
            method: 'post',
            context: this
        }).done(function (utilisateur) {
        	console.log(utilisateur);
        	var html = '<div class="alert alert-success" role="alert">' 
       		 +'Candidat Inscrit !'
    		 +'</div>';
        	
        	$(this).append(html);
        	$(this).children('input').val('');
        });

    });
    
    
